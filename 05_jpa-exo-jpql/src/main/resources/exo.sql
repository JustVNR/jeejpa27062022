-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- H�te : 127.0.0.1:3306
-- G�n�r� le : mar. 28 juin 2022 � 16:28
-- Version du serveur : 8.0.27
-- Version de PHP : 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de donn�es : `jpa-jpql2`
--

-- --------------------------------------------------------

--
-- Structure de la table `auteurs`
--

DROP TABLE IF EXISTS `auteurs`;
CREATE TABLE IF NOT EXISTS `auteurs` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `deces` date DEFAULT NULL,
  `naissance` date NOT NULL,
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(50) NOT NULL,
  `version` int NOT NULL,
  `nationalite_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKeam1cibjxkjpj7kmbfcoseqkg` (`nationalite_id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb3;

--
-- D�chargement des donn�es de la table `auteurs`
--

INSERT INTO `auteurs` (`id`, `deces`, `naissance`, `nom`, `prenom`, `version`, `nationalite_id`) VALUES
(1, NULL, '1948-03-04', 'Ellroy', 'James', 0, 1),
(2, NULL, '1947-09-21', 'King', 'Stephen', 0, 1),
(3, '1986-02-11', '1920-10-08', 'Herbert', 'Frank', 0, 1),
(4, '1982-03-02', '1928-12-16', 'Dick', 'Philip K.', 0, 1),
(5, '2004-11-09', '1954-08-15', 'Larsson', 'Stieg', 0, 2),
(6, '1964-08-12', '1908-05-28', 'Fleming', 'Ian', 0, 3),
(7, '1989-01-16', '1906-04-28', 'Boileau', 'Pierre Louis', 0, 4),
(8, '1998-06-07', '1908-07-03', 'Narcejac', 'Thomas', 0, 4),
(9, '1616-04-23', '1547-09-29', 'de Cervantes', 'Miguel', 0, 5),
(10, '1972-01-28', '1906-10-16', 'Buzzati', 'Dino', 0, 6),
(11, '2016-02-19', '1932-01-05', 'Eco', 'Umberto', 0, 6),
(12, '1910-11-20', '1828-09-09', 'Tolsto�', 'L�on', 0, 7),
(13, '1914-02-25', '1874-06-01', 'Souvestre', 'Pierre', 0, 4),
(14, '1969-08-25', '1885-09-15', 'Allain', 'Marcel', 0, 4),
(15, NULL, '1965-07-31', 'Rowling', 'J. K.', 0, 3),
(16, '1973-09-02', '1892-01-03', 'Tolkien', 'J. R. R.', 0, 3),
(17, '1946-08-13', '1866-09-21', 'Wells', 'H. G.', 0, 3),
(18, '1994-01-31', '1912-02-20', 'Boulle', 'Pierre', 0, 4),
(19, '1916-11-22', '1876-01-12', 'London', 'Jack', 0, 1),
(20, '1937-03-15', '1890-08-20', 'Lovecraft', 'H P', 0, 1),
(21, '1902-09-29', '1840-04-02', 'Zola', '�mile', 0, 4),
(22, NULL, '1948-03-17', 'Gibson', 'William', 0, 1),
(23, '1962-03-03', '1886-07-16', 'Benoit', 'Pierre', 0, 4),
(24, '2012-06-05', '1920-08-22', 'Bradbury', 'Ray', 0, 1),
(25, '1963-11-22', '1894-07-26', 'Huxley', 'Aldous', 0, 3),
(26, '1995-06-03', '1942-12-19', 'Manchette', 'Jean-Patrick', 0, 4),
(27, NULL, '1937-02-04', 'Bastid', 'Jean-Pierre', 0, 4),
(28, '1910-04-21', '1835-11-30', 'Twain', 'Mark', 0, 1),
(29, '2001-05-11', '1952-03-11', 'Adams', 'Douglas', 0, 3),
(30, '1924-06-03', '1883-07-03', 'Kafka	', 'Franz', 0, 8),
(31, '1950-01-21', '1903-06-25', 'Orwell', 'George', 0, 3),
(32, '1989-09-04', '1903-02-13', 'Simenon', 'Georges', 0, 9),
(33, '1885-05-22', '1802-02-26', 'Hugo', 'Victor', 0, 4),
(34, '1849-10-07', '1809-01-19', 'Poe', 'Edgar Allan', 0, 1),
(35, '2020-12-12', '1931-10-19', 'Le Carr�', 'John ', 0, 3),
(36, NULL, '1959-10-31', 'Stephenson', 'Neal', 0, 1);

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NOT NULL,
  `version` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb3;

--
-- D�chargement des donn�es de la table `categories`
--

INSERT INTO `categories` (`id`, `nom`, `version`) VALUES
(1, 'Policier', 0),
(2, 'Autobiographie', 0),
(3, 'Horreur', 0),
(4, 'Fantastique', 0),
(5, 'Thriller', 0),
(6, 'Science-fiction', 0),
(7, 'Drame', 0),
(8, 'Espionnage', 0),
(9, 'Historique', 0),
(10, 'Politique', 0),
(11, 'Fantasy', 0),
(12, 'Naturaliste', 0),
(13, 'Anticipation', 0),
(14, 'Aventure', 0),
(15, 'Cyberpunk', 0),
(16, 'Humour', 0),
(17, 'Steampunk', 0),
(18, 'Fable', 0);

-- --------------------------------------------------------

--
-- Structure de la table `livres`
--

DROP TABLE IF EXISTS `livres`;
CREATE TABLE IF NOT EXISTS `livres` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `annee_sortie` int NOT NULL,
  `titre` varchar(255) NOT NULL,
  `version` int NOT NULL,
  `categorie_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKfnlv2rmpf229lropillhdqgqc` (`categorie_id`)
) ENGINE=InnoDB AUTO_INCREMENT=143 DEFAULT CHARSET=utf8mb3;

--
-- D�chargement des donn�es de la table `livres`
--

INSERT INTO `livres` (`id`, `annee_sortie`, `titre`, `version`, `categorie_id`) VALUES
(1, 1982, 'Lune sanglante', 0, 1),
(2, 1987, 'Le Dahlia noir', 0, 1),
(3, 1990, 'L.A. Confidential', 0, 1),
(4, 1995, 'American Tablo�d', 0, 1),
(5, 2001, 'American Death Trip', 0, 1),
(6, 2009, 'Underworld USA ', 0, 1),
(7, 1997, 'Ma part d\'ombre', 0, 2),
(8, 1974, 'Carrie', 0, 3),
(9, 1977, 'Shining,l\'enfant lumi�re', 0, 3),
(10, 1978, 'Le Fl�au', 0, 3),
(11, 1979, 'Dead Zone', 0, 4),
(12, 1983, 'Christine', 0, 3),
(13, 1988, '�a', 0, 3),
(14, 1987, 'Misery', 0, 5),
(15, 1965, 'Dune', 0, 6),
(16, 1969, 'Le Messie de Dune', 0, 6),
(17, 1978, 'Les Enfants de Dune', 0, 6),
(18, 1981, 'L\'Empereur-Dieu de Dune', 0, 6),
(19, 1985, 'Les H�r�tiques de Dune', 0, 6),
(20, 1986, 'La Maison des m�res', 0, 6),
(21, 1968, 'Les andro�des r�vent-ils de moutons �lectriques ?	', 0, 6),
(22, 1969, 'Ubik', 0, 6),
(23, 1978, 'Substance Mort', 0, 6),
(24, 1962, 'Le Ma�tre du Haut Ch�teau	', 0, 6),
(25, 1975, 'Les Marteaux de Vulcain', 0, 4),
(26, 2005, 'Les Hommes qui n\'aimaient pas les femmes', 0, 1),
(27, 2005, 'La Fille qui r�vait d\'un bidon d`\'essence et d\'une allumette', 0, 1),
(28, 2005, 'La Reine dans le palais des courants d\'air', 0, 1),
(29, 1953, 'Casino Royale', 0, 8),
(30, 1954, 'Live and Let Die', 0, 8),
(31, 1955, 'Moonraker', 0, 8),
(32, 1956, 'Diamonds Are Forever', 0, 8),
(33, 1957, 'From Russia With Love', 0, 8),
(34, 1958, 'Dr. No', 0, 8),
(35, 1959, 'Goldfinger', 0, 8),
(36, 1960, 'For Your Eyes Only', 0, 8),
(37, 1961, 'Thunderball', 0, 8),
(38, 1962, 'The Spy Who Loved Me', 0, 8),
(39, 1963, 'On Her Majesty\'s Secret Service', 0, 8),
(40, 1964, 'You Only Live Twice', 0, 8),
(41, 1965, 'The Man With The Golden Gun', 0, 8),
(42, 1966, 'Octopussy and the Living Daylights', 0, 8),
(43, 1605, 'Don Quichotte', 0, 14),
(44, 1940, 'Le D�sert des Tartares', 0, 7),
(45, 1966, 'Le K', 0, 4),
(46, 1980, 'Le Nom de la rose', 0, 7),
(47, 1863, 'Les Cosaques', 0, 9),
(48, 1865, 'Guerre et Paix', 0, 9),
(49, 1877, 'Anna Kar�nine', 0, 7),
(50, 1899, 'R�surrection', 0, 10),
(51, 1997, 'Harry Potter � l\'�cole des sorciers', 0, 11),
(52, 1998, 'Harry Potter et la Chambre des secrets', 0, 11),
(53, 1999, 'Harry Potter et le Prisonnier d\'Azkaban', 0, 11),
(54, 1999, 'Harry Potter et la Coupe de feu', 0, 11),
(55, 2003, 'Harry Potter et l\'Ordre du ph�nix', 0, 11),
(56, 2005, 'Harry Potter et le Prince de sang-m�l�', 0, 11),
(57, 2007, 'Harry Potter et les Reliques de la Mort', 0, 11),
(58, 1949, 'Le Fermier Gilles de Ham	', 0, 11),
(59, 1937, 'Le Hobbit', 0, 11),
(60, 1954, 'La Communaut� de l\'Anneau', 0, 11),
(61, 1954, 'Les Deux Tours', 0, 11),
(62, 1955, 'Le Retour du roi', 0, 11),
(63, 1885, 'La Machine � explorer le temps', 0, 6),
(64, 1896, 'L\'�le du docteur Moreau', 0, 6),
(65, 1897, 'L\'Homme invisible', 0, 6),
(66, 1898, 'La Guerre des mondes', 0, 6),
(67, 1963, 'La Plan�te des singes', 0, 6),
(68, 1903, 'L\'Appel de la for�t', 0, 14),
(69, 1923, 'Croc-Blanc', 0, 14),
(70, 1963, 'Le Bureau des assassinats', 0, 1),
(71, 1913, 'Le Cabaret de la derni�re chance', 0, 2),
(72, 1928, 'L\'Appel de Cthulhu', 0, 4),
(73, 1941, 'L\'Affaire Charles Dexter Ward', 0, 4),
(74, 1931, 'Le Cauchemar d\'Innsmouth', 0, 3),
(75, 1936, 'Celui qui hantait les t�n�bres', 0, 4),
(76, 1923, 'La Peur qui r�de', 0, 4),
(77, 1926, 'Le Mod�le de Pickman', 0, 4),
(78, 1871, 'La Fortune des Rougon', 0, 12),
(79, 1872, 'La Cur�e', 0, 12),
(80, 1873, 'Le Ventre de Paris', 0, 12),
(81, 1874, 'La Conqu�te de Plassans', 0, 12),
(82, 1875, 'La Faute de l\'abb� Mouret', 0, 12),
(83, 1876, 'Son Excellence Eug�ne Rougon', 0, 12),
(84, 1878, 'L\'Assommoir', 0, 12),
(85, 1880, 'Une page d\'amour', 0, 12),
(86, 1882, 'Nana', 0, 12),
(87, 1883, 'Pot-Bouille', 0, 12),
(88, 1883, 'Au Bonheur des Dames', 0, 12),
(89, 1885, 'La Joie de vivre', 0, 12),
(90, 1886, 'Germinal', 0, 12),
(91, 1887, 'L\'�uvre', 0, 12),
(92, 1888, 'La Terre', 0, 12),
(93, 1890, 'Le R�ve', 0, 12),
(94, 1891, 'L\'Argent', 0, 12),
(95, 1892, 'La D�b�cle', 0, 12),
(96, 1893, 'Le Docteur Pascal', 0, 12),
(97, 1984, 'Neuromancien', 0, 15),
(98, 1986, 'Comte Z�ro', 0, 15),
(99, 1988, 'Mona Lisa s\'�clate', 0, 15),
(100, 1919, 'L\'Atlantide', 0, 14),
(101, 1950, 'Chroniques martiennes', 0, 6),
(102, 1955, 'Fahrenheit 451', 0, 6),
(103, 1932, 'Le Meilleur des mondes', 0, 13),
(104, 1972, 'L\'Affaire N\'Gustro', 0, 1),
(105, 1972, 'Nada', 0, 1),
(106, 1972, 'Morgue pleine', 0, 1),
(107, 1976, 'Que d\'os !', 0, 1),
(108, 1976, 'Le Petit Bleu de la c�te ouest', 0, 1),
(109, 1981, 'La Position du tireur couch�', 0, 1),
(110, 1884, 'Les aventures de Huckleberry Finn', 0, 14),
(111, 1876, 'Les aventures de Tom Sawyer', 0, 14),
(112, 1982, 'Le Guide du voyageur galactique', 0, 6),
(113, 1982, 'Le Dernier Restaurant avant la fin du monde', 0, 6),
(114, 1983, 'La Vie, l\'Univers et le Reste', 0, 6),
(115, 1994, 'Salut, et encore merci pour le poisson', 0, 6),
(116, 1994, 'Globalement inoffensive', 0, 6),
(117, 1925, 'Le Proc�s', 0, 7),
(118, 1949, '1984', 0, 6),
(119, 1938, 'La Marie du port', 0, 1),
(120, 1949, 'Les Fant�mes du chapelier', 0, 1),
(121, 1945, 'L\'A�n� des Ferchaux', 0, 1),
(122, 1942, 'La Veuve Couderc', 0, 1),
(123, 1938, 'L\'Homme qui regardait passer les trains', 0, 1),
(124, 1931, 'Le Relais d\'Alsace', 0, 1),
(125, 1933, 'La Maison du canal', 0, 1),
(126, 1931, 'Le Chien jaune', 0, 1),
(127, 1931, 'Au rendez-vous des Terre-Neuvas', 0, 1),
(128, 1932, 'L\'Affaire Saint-Fiacre', 0, 1),
(129, 1933, 'L\'�cluse num�ro 1', 0, 1),
(130, 1931, 'L\'Ombre chinoise', 0, 1),
(131, 1931, 'Un crime en Hollande', 0, 1),
(132, 1952, 'Celle qui n\'�tait plus', 0, 1),
(133, 1953, 'Les Visages de l\'ombre', 0, 1),
(134, 1959, 'L\'ing�nieur aimait trop les chiffres', 0, 1),
(135, 1954, 'D\'entre les morts', 0, 1),
(136, 1911, 'Fant�mas', 0, 1),
(137, 1911, 'Juve contre Fant�mas', 0, 1),
(138, 1911, 'Le Mort qui tue', 0, 1),
(139, 1912, 'L\'Assassin de Lady Beltham', 0, 1),
(140, 1913, 'La Cravate de chanvre', 0, 1),
(141, 1971, 'Laissez bronzer les cadavres !', 0, 1),
(142, 1992, 'Le Samoura� virtuel', 0, 15);

-- --------------------------------------------------------

--
-- Structure de la table `livres_auteurs`
--

DROP TABLE IF EXISTS `livres_auteurs`;
CREATE TABLE IF NOT EXISTS `livres_auteurs` (
  `livres_id` bigint NOT NULL,
  `auteurs_id` bigint NOT NULL,
  KEY `FK360sta7iximrwtxuv190mldqq` (`auteurs_id`),
  KEY `FKpkha04618mktl8qbxq24vgj5y` (`livres_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- D�chargement des donn�es de la table `livres_auteurs`
--

INSERT INTO `livres_auteurs` (`livres_id`, `auteurs_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 2),
(9, 2),
(10, 2),
(11, 2),
(12, 2),
(13, 2),
(14, 2),
(15, 3),
(16, 3),
(17, 3),
(18, 3),
(19, 3),
(20, 3),
(21, 4),
(22, 4),
(23, 4),
(24, 4),
(25, 4),
(26, 5),
(27, 5),
(28, 5),
(29, 6),
(30, 6),
(31, 6),
(32, 6),
(33, 6),
(34, 6),
(35, 6),
(36, 6),
(37, 6),
(38, 6),
(39, 6),
(40, 6),
(41, 6),
(42, 6),
(43, 9),
(44, 10),
(45, 10),
(46, 11),
(47, 12),
(48, 12),
(49, 12),
(50, 12),
(51, 15),
(52, 15),
(53, 15),
(54, 15),
(55, 15),
(56, 15),
(57, 15),
(58, 16),
(59, 16),
(60, 16),
(61, 16),
(62, 16),
(63, 17),
(64, 17),
(65, 17),
(66, 17),
(67, 18),
(68, 19),
(69, 19),
(70, 19),
(71, 19),
(72, 20),
(73, 20),
(74, 20),
(75, 20),
(76, 20),
(77, 20),
(78, 21),
(79, 21),
(80, 21),
(81, 21),
(82, 21),
(83, 21),
(84, 21),
(85, 21),
(86, 21),
(87, 21),
(88, 21),
(89, 21),
(90, 21),
(91, 21),
(92, 21),
(93, 21),
(94, 21),
(95, 21),
(96, 21),
(97, 22),
(98, 22),
(99, 22),
(100, 23),
(101, 24),
(102, 24),
(103, 25),
(104, 26),
(105, 26),
(106, 26),
(107, 26),
(108, 26),
(109, 26),
(110, 28),
(111, 28),
(112, 29),
(113, 29),
(114, 29),
(115, 29),
(116, 29),
(117, 30),
(118, 31),
(119, 32),
(120, 32),
(121, 32),
(122, 32),
(123, 32),
(124, 32),
(125, 32),
(126, 32),
(127, 32),
(128, 32),
(129, 32),
(130, 32),
(131, 32),
(132, 7),
(132, 8),
(133, 7),
(133, 8),
(134, 7),
(134, 8),
(135, 7),
(135, 8),
(136, 13),
(136, 14),
(137, 13),
(137, 14),
(138, 13),
(138, 14),
(139, 13),
(139, 14),
(140, 13),
(140, 14),
(141, 26),
(141, 27),
(142, 36);

-- --------------------------------------------------------

--
-- Structure de la table `nations`
--

DROP TABLE IF EXISTS `nations`;
CREATE TABLE IF NOT EXISTS `nations` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `nom` varchar(100) NOT NULL,
  `version` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb3;

--
-- D�chargement des donn�es de la table `nations`
--

INSERT INTO `nations` (`id`, `nom`, `version`) VALUES
(1, '�tats-Unis', 0),
(2, 'Su�de', 0),
(3, 'Grande-Bretagne', 0),
(4, 'France', 0),
(5, 'Espagne', 0),
(6, 'Italie', 0),
(7, 'Russie', 0),
(8, 'Tch�coslovaquie', 0),
(9, 'Belgique', 0);

--
-- Contraintes pour les tables d�charg�es
--

--
-- Contraintes pour la table `auteurs`
--
ALTER TABLE `auteurs`
  ADD CONSTRAINT `FKeam1cibjxkjpj7kmbfcoseqkg` FOREIGN KEY (`nationalite_id`) REFERENCES `nations` (`id`);

--
-- Contraintes pour la table `livres`
--
ALTER TABLE `livres`
  ADD CONSTRAINT `FKfnlv2rmpf229lropillhdqgqc` FOREIGN KEY (`categorie_id`) REFERENCES `categories` (`id`);

--
-- Contraintes pour la table `livres_auteurs`
--
ALTER TABLE `livres_auteurs`
  ADD CONSTRAINT `FK360sta7iximrwtxuv190mldqq` FOREIGN KEY (`auteurs_id`) REFERENCES `auteurs` (`id`),
  ADD CONSTRAINT `FKpkha04618mktl8qbxq24vgj5y` FOREIGN KEY (`livres_id`) REFERENCES `livres` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;