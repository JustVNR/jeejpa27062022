package fr.dawan.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name="livres")
public class Livre {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@Version
	private int version;
	
	@Column(nullable = false, length = 255)
	private String titre;
	
	@Column(name="annee_sortie")
	private int anneSortie;
	
	@ManyToOne
	private Categorie categorie;
	
	@ManyToMany
	private List<Auteur> auteurs = new ArrayList<>();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public int getAnneSortie() {
		return anneSortie;
	}

	public void setAnneSortie(int anneSortie) {
		this.anneSortie = anneSortie;
	}

	public Categorie getCategorie() {
		return categorie;
	}

	public void setCategorie(Categorie categorie) {
		this.categorie = categorie;
	}

	public List<Auteur> getAuteurs() {
		return auteurs;
	}

	public void setAuteurs(List<Auteur> auteurs) {
		this.auteurs = auteurs;
	}

	public Livre() {
		super();
	}

	public Livre(String titre, int anneSortie) {
		super();
		this.titre = titre;
		this.anneSortie = anneSortie;
	}

	public Livre(String titre, int anneSortie, Categorie categorie) {
		super();
		this.titre = titre;
		this.anneSortie = anneSortie;
		this.categorie = categorie;
	}

	@Override
	public String toString() {
		return "Livre [id = " + id + ", titre = " + titre + ", anneSortie = " + anneSortie
				+ ", categorie=" + categorie + "]";
	}
}
