package fr.dawan.dao;

import java.util.List;

import javax.persistence.EntityManager;

import fr.dawan.entities.Auteur;
import fr.dawan.entities.Categorie;
import fr.dawan.entities.Livre;

public class LivreDAO extends GenericDAO<Livre> {

	public LivreDAO(EntityManager em) {
		super(em, Livre.class);

	}

	public List<Livre> findByAnnee(int annee) {

		return em.createQuery("SELECT l FROM Livre l WHERE l.anneSortie = :annee", Livre.class)
				.setParameter("annee", annee).getResultList();
	}

	public List<Livre> findByAuteur(Auteur auteur) {
		return em.createQuery("SELECT l FROM Livre l JOIN l.auteurs a WHERE a = :auteur ", Livre.class)
				.setParameter("auteur", auteur).getResultList();
	}

	public List<Livre> findByCategorie(Categorie categorie) {
		return em.createQuery("SELECT l FROM Livre l WHERE l.categorie = :categorie", Livre.class)
				.setParameter("categorie", categorie).getResultList();
	}

	public Long countByPeriod(int min, int max) {
		return em.createQuery("SELECT COUNT(l) FROM Livre l WHERE l.anneSortie BETWEEN :min AND :max", Long.class)
				.setParameter("min", min).setParameter("max", max).getSingleResult();
	}

	public List<Livre> GetMultiAuteurs() {

		return em.createQuery("SELECT l FROM Livre l WHERE SIZE(l.auteurs) > 1", Livre.class).getResultList();
	}


}
