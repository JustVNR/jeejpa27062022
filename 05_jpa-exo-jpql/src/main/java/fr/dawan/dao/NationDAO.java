package fr.dawan.dao;

import javax.persistence.EntityManager;

import fr.dawan.entities.Nation;

public class NationDAO extends GenericDAO<Nation> {

	public NationDAO(EntityManager em) {
		super(em, Nation.class);
	}

	public Nation findByNom(String nom) {
		return em.createQuery("SELECT n FROM Nation n WHERE n.nom = :nom", Nation.class).setParameter("nom", nom)
				.getSingleResult();
	}
}
