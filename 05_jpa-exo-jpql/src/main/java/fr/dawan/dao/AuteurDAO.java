package fr.dawan.dao;

import java.util.List;

import javax.persistence.EntityManager;

import fr.dawan.entities.Auteur;
import fr.dawan.entities.Nation;

public class AuteurDAO extends GenericDAO<Auteur> {

	public AuteurDAO(EntityManager em) {
		super(em, Auteur.class);
	}

	public Auteur findByNameAndFirstName(String nom, String prenom) {

		return em.createQuery("SELECT a FROM Auteur a WHERE a.nom = :nom AND a.prenom = :prenom", Auteur.class)
				.setParameter("nom", nom).setParameter("prenom", prenom).getSingleResult();
	}

	public List<Auteur> GetAlive() {
		return em.createQuery("SELECT a FROM Auteur a WHERE a.deces IS NULL", Auteur.class).getResultList();
	}

	public List<Auteur> GetNoBook() {
//		return em.createQuery("SELECT a FROM Auteur a LEFT JOIN a.livres l WHERE l IS NULL", Auteur.class)
//				.getResultList();

//		return em.createQuery("SELECT a FROM Auteur a WHERE SIZE(a.livres) = 0", Auteur.class)
//				.getResultList();

		return em.createQuery("SELECT a FROM Auteur a WHERE a.livres IS EMPTY", Auteur.class).getResultList();
	}

	public List<Auteur> findByNation(Nation nation) {

		return em.createQuery("SELECT a FROM Auteur a WHERE a.nationalite = :nation ", Auteur.class)
				.setParameter("nation", nation).getResultList();
	}
}
