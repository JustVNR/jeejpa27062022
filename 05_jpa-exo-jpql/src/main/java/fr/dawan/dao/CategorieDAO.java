package fr.dawan.dao;

import javax.persistence.EntityManager;

import fr.dawan.entities.Categorie;

public class CategorieDAO extends GenericDAO<Categorie> {

	public CategorieDAO(EntityManager em) {
		super(em, Categorie.class);
	}

	public Categorie findByNom(String nom) {
		return em.createQuery("SELECT c FROM Categorie c WHERE c.nom = :nom", Categorie.class).setParameter("nom", nom)
				.getSingleResult();
	}
}
