package fr.dawan.main;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import fr.dawan.dao.AuteurDAO;
import fr.dawan.dao.CategorieDAO;
import fr.dawan.dao.LivreDAO;
import fr.dawan.dao.NationDAO;
import fr.dawan.entities.Auteur;
import fr.dawan.entities.Categorie;
import fr.dawan.entities.Livre;
import fr.dawan.entities.Nation;

public class Dao {

	public static void main(String[] args) {

		EntityManagerFactory emf = null;

		EntityManager em = null;

		try {

			emf = Persistence.createEntityManagerFactory("jpa-exo");

			em = emf.createEntityManager();

			LivreDAO livreDAO = new LivreDAO(em);
			CategorieDAO categorieDAO = new CategorieDAO(em);
			AuteurDAO auteurDAO = new AuteurDAO(em);
			NationDAO nationDAO = new NationDAO(em);

			livreDAO.findByAnnee(2005).forEach(System.out::println);

			System.out.println(" ---------------- FIND ALL --------------------");

			livreDAO.findAll().forEach(System.out::println);

			System.out.println(" ---------------- FIND BY ID --------------------");

			System.out.println(" ---------------- SAVE --------------------");

			Categorie categorie = categorieDAO.findById(1L);

			Livre livre = new Livre("Super titre", 2022, categorie);

			livreDAO.saveOrUpdate(livre);

			// Nombre de livres
			// livreDAO.remove(1L);
			System.out.println(" ---------------- COUNT --------------------");
			System.out.println(livreDAO.count());
			
			// Livres parus en 1962
			System.out.println(" ---------------- FIND BY ANNEE --------------------");
			livreDAO.findByAnnee(1962).forEach(System.out::println);

			// Livres de James Ellroy
			System.out.println(" ---------------- Livres de James Ellroy --------------------");
			Auteur james = auteurDAO.findByNameAndFirstName("Ellroy", "James");
			livreDAO.findByAuteur(james).forEach(System.out::println);
		
			// Livres de "Science-fiction"
			System.out.println(" ---------------- Livres de 'Science-fiction' --------------------");
			Categorie scienceFic = categorieDAO.findByNom("Science-fiction");
			livreDAO.findByCategorie(scienceFic).forEach(System.out::println);

			// Nombre de livres parus entre 1970 et 1980
			System.out.println(" ---------------- Nombre de livres parus entre 1970 et 1980 --------------------");
			System.out.println(livreDAO.countByPeriod(1970, 1980));

			// Livres et ayant plusieurs auteurs
			System.out.println(" ---------------- Livres et ayant plusieurs auteurs --------------------");
			livreDAO.GetMultiAuteurs().forEach(System.out::println);
			// Auteurs encore vivants
			System.out.println(" ---------------- Auteurs encore vivants --------------------");
			auteurDAO.GetAlive().forEach(System.out::println);
			// Auteurs sans livres
			System.out.println(" ---------------- Auteurs sans livres --------------------");
			auteurDAO.GetNoBook().forEach(System.out::println);
			
			// Auteurs britanniques
			System.out.println(" ---------------- Auteurs britanniques --------------------");
			Nation british  = nationDAO.findByNom("Grande-Bretagne");
			auteurDAO.findByNation(british).forEach(System.out::println);

			
		} catch (Exception e) {

			e.printStackTrace();
			e.getMessage();

		} finally {
			if (em != null) {
				em.close();
			}
			if (emf != null) {
				emf.close();
			}
		}
	}
}
