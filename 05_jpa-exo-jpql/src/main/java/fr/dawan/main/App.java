package fr.dawan.main;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import fr.dawan.entities.Auteur;
import fr.dawan.entities.Livre;

public class App {

	public static void main(String[] args) {

		EntityManagerFactory emf = null;

		EntityManager em = null;

		try {

			emf = Persistence.createEntityManagerFactory("jpa-exo");

			em = emf.createEntityManager();

			// Livres sortis une certaine ann�e pass�e en param�tre

			em.createQuery("SELECT l FROM Livre l WHERE l.anneSortie = :annee", Livre.class).setParameter("annee", 2005)
					.getResultList().forEach(System.out::println);

			System.out.println(" ----------------------------- ");
			// Livres sortis apr�s et avant 2 ann�es pass�es en param�tres
			em.createQuery("SELECT l FROM Livre l WHERE l.anneSortie BETWEEN :anneeMin AND :anneeMax", Livre.class)
			.setParameter("anneeMin", 1980)
			.setParameter("anneeMax", 1986)
			.getResultList().forEach(System.out::println);

			System.out.println(" ----------------------------- ");
			// Livres sortis en 1962, 1988 ou 1992
			em.createQuery("SELECT l FROM Livre l WHERE l.anneSortie IN(1962, 1988, 1992)", Livre.class)
			.getResultList().forEach(System.out::println);

			System.out.println(" ----------------------------- ");
			// Livres dont le titre commence par "D" et dont la 3i�me lettre est un "a"
			em.createQuery("SELECT l FROM Livre l WHERE l.titre LIKE 'D_a%'", Livre.class)
			.getResultList().forEach(System.out::println);

			System.out.println(" ----------------------------- ");
			// Nombre de livres sortis l'ann�e pass�e en param�tre

			long nombreLivre = em.createQuery("SELECT COUNT(l) FROM Livre l WHERE l.anneSortie = :annee", Long.class).setParameter("annee", 2005)
			.getSingleResult();
			
			System.out.println(nombreLivre);

			System.out.println(" ----------------------------- ");
			// Nombre d'auteurs toujours vivants
			
			System.out.println(em.createQuery("SELECT COUNT(a) FROM Auteur a WHERE a.deces IS NULL", Long.class)
			.getSingleResult());
			

			System.out.println(" ----------------------------- ");
			// Nombre de livres sortis par ann�e,
			// group�s par ann�e de sortie,
			// ordonn�s des plus au moins r�cents
			// pr�sent�s sous formes de chaines de caract�res du type "2009->1"

			em.createQuery(" SELECT CONCAT(l.anneSortie, '->', COUNT(l)) FROM Livre l GROUP BY l.anneSortie ORDER BY l.anneSortie DESC", String.class)
			.getResultList().forEach(System.out::println);
			
			System.out.println(" ----------------------------- ");
			// Noms complets des auteurs pr�sent�s sous la forme "prenom nom"

			em.createQuery("SELECT CONCAT(a.prenom, ' ', a.nom) FROM Auteur a", String.class)
			.getResultList().forEach(System.out::println);
			
			System.out.println(" ----------------------------- ");
			// 2 premi�res lettres du nom de chaque auteur, et id de l'auteur
			// pr�sent� sous forme de chaine de caract�res du type "Bo-12"

			em.createQuery("SELECT CONCAT(SUBSTRING(a.nom, 1, 2), '-', a.id) FROM Auteur a", String.class)
			.getResultList().forEach(System.out::println);
			System.out.println(" ----------------------------- ");
			// Nombre de pr�noms diff�rents parmi les auteurs
			System.out.println(em.createQuery("SELECT DISTINCT COUNT(a.prenom) FROM Auteur a", Long.class)
					.getSingleResult());
			
			System.out.println(" ----------------------------- ");
			// Livres sortis dans la decennie de l'ann�e pass�e param�tres ordonn�s du plus
			// vieux au plus r�cent
			em.createQuery("SELECT l FROM Livre l WHERE l.anneSortie BETWEEN :annee-MOD(:annee, 10) AND :annee+9-MOD(:annee, 10) ORDER BY l.anneeSortie", Livre.class)
			.setParameter("annee", 1985)
			.getResultList().forEach(System.out::println);
		} catch (Exception e) {

			e.printStackTrace();
			e.getMessage();

		} finally {
			if (em != null) {
				em.close();
			}
			if (emf != null) {
				emf.close();
			}
		}
	}
}
