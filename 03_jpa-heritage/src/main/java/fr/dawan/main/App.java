package fr.dawan.main;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import fr.dawan.entities.CompteBancaire;
import fr.dawan.entities.CompteEpargne;
import fr.dawan.entities.CompteTitre;

public class App {

	public static void main(String[] args) {

		EntityManagerFactory emf = null;

		EntityManager em = null;
		EntityTransaction tx = null;

		try {

			emf = Persistence.createEntityManagerFactory("jpa-heritage");

			em = emf.createEntityManager();

			tx = em.getTransaction();

			tx.begin();
			
			//CompteBancaire cb1 = new CompteBancaire("riri duck", "fr457854", 500.0);
			CompteBancaire ce1 = new CompteEpargne(0.5, "fifi duck", "fr47854", 1500.0);
			CompteBancaire ce2 = new CompteEpargne(0.2, "loulou duck", "fr832854", 2500.0);
			
			CompteBancaire ct1 = new CompteTitre(50, "donald duck", "en654784", 2500.0);
			
			//em.persist(cb1);
			em.persist(ce1);
			em.persist(ce2);
			em.persist(ct1);
			
			tx.commit();

		} catch (Exception e) {

			e.printStackTrace();
			e.getMessage();
			tx.rollback();
		} finally {
			if (em != null) {
				em.close();
			}
			if (emf != null) {
				emf.close();
			}
		}
	}
}
