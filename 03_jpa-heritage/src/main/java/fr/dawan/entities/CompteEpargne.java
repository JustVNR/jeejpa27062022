package fr.dawan.entities;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
//@DiscriminatorValue("Ce")
public class CompteEpargne extends CompteBancaire {

	private double taux = 0.05;

	public double getTaux() {
		return taux;
	}

	public void setTaux(double taux) {
		this.taux = taux;
	}

	public CompteEpargne() {
		super();
	}

	public CompteEpargne(String titulaire, String iban, double solde) {
		super(titulaire, iban, solde);
	}
	
	public CompteEpargne(double taux, String titulaire, String iban, double solde) {
		super(titulaire, iban, solde);
		this.taux = taux;
		
	}

	public void calculSolde() {
		super.solde *= (1 + taux * 100);
	}

	@Override
	public String toString() {
		return "CompteEpargne [taux=" + taux + ", toString()=" + super.toString() + "]";
	}

}
