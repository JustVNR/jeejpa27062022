package fr.dawan.entities;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

//@Entity
//@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
//@DiscriminatorColumn(name = "typeCompte")
//@DiscriminatorValue("Cb")

// @Inheritance(strategy = InheritanceType.JOINED)

//@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)


@MappedSuperclass
public class CompteBancaire {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	private String titulaire;

	private String iban;

	protected double solde;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitulaire() {
		return titulaire;
	}

	public void setTitulaire(String titulaire) {
		this.titulaire = titulaire;
	}

	public String getIban() {
		return iban;
	}

	public void setIban(String iban) {
		this.iban = iban;
	}

	public double getSolde() {
		return solde;
	}

	public void setSolde(double solde) {
		this.solde = solde;
	}

	public CompteBancaire() {
		super();
	}

	public CompteBancaire(String titulaire, String iban, double solde) {
		super();
		this.titulaire = titulaire;
		this.iban = iban;
		this.solde = solde;
	}

	public void crediter(double valeur) {
		if (valeur > 0.0) {
			solde += valeur;
		}
	}
	
	public void debiter(double valeur) {
		if (valeur > 0.0 && solde >= valeur) {
			solde -= valeur;
		}
	}

	@Override
	public String toString() {
		return "CompteBancaire [id=" + id + ", titulaire=" + titulaire + ", iban=" + iban + ", solde=" + solde + "]";
	}
}
