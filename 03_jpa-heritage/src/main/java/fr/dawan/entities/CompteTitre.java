package fr.dawan.entities;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
//@DiscriminatorValue("Ct")
public class CompteTitre extends CompteBancaire{

	private double plafond;

	public double getPlafond() {
		return plafond;
	}

	public void setPlafond(double plafond) {
		this.plafond = plafond;
	}

	public CompteTitre() {
		super();
	}
	
	public CompteTitre(String titulaire, String iban, double solde) {
		super(titulaire, iban, solde);
	}
	
	public CompteTitre(double plafond, String titulaire, String iban, double solde) {
		super(titulaire, iban, solde);
		this.plafond = plafond;
	}

	@Override
	public String toString() {
		return "CompteTitre [plafond=" + plafond + ", toString()=" + super.toString() + "]";
	}
}
