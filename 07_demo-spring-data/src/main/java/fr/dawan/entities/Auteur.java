package fr.dawan.entities;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "auteurs")
public class Auteur {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Version
	private int version;

	@Column(nullable = false, length = 50)
	private String nom;

	@Column(nullable = false, length = 50)
	private String prenom;

	private LocalDate deces;

	private LocalDate naissance;

	@ManyToOne
	private Nation nationalite;

	@ManyToMany(mappedBy = "auteurs")
	private List<Livre> livres = new ArrayList<>();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public LocalDate getDeces() {
		return deces;
	}

	public void setDeces(LocalDate deces) {
		this.deces = deces;
	}

	public LocalDate getNaissance() {
		return naissance;
	}

	public void setNaissance(LocalDate naissance) {
		this.naissance = naissance;
	}

	public Nation getNationalite() {
		return nationalite;
	}

	public void setNationalite(Nation nationalite) {
		this.nationalite = nationalite;
	}

	public List<Livre> getLivres() {
		return livres;
	}

	public void setLivres(List<Livre> livres) {
		this.livres = livres;
	}

	public Auteur() {
		super();
	}

	public Auteur(String nom, String prenom, LocalDate deces, LocalDate naissance) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.deces = deces;
		this.naissance = naissance;
	}

	@Override
	public String toString() {
		return "Auteur [id=" + id + ", version=" + version + ", nom=" + nom + ", prenom=" + prenom + ", deces=" + deces
				+ ", naissance=" + naissance + ", nationalite=" + nationalite + "]";
	}
}
