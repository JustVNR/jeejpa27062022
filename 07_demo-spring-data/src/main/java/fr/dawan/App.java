package fr.dawan;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import fr.dawan.dao.AuteurDAO;
import fr.dawan.dao.CategorieDAO;
import fr.dawan.dao.LivreDAO;
import fr.dawan.dao.NationDAO;
import fr.dawan.entities.Auteur;
import fr.dawan.entities.Categorie;
import fr.dawan.entities.Nation;

@SpringBootApplication
public class App implements CommandLineRunner{

	@Autowired
	private AuteurDAO _auteurDAO;
	
	@Autowired
	private LivreDAO _livreDAO;
	
	@Autowired
	private CategorieDAO _categorieDAO;
	
	@Autowired
	private NationDAO _nationDAO;
	public static void main(String[] args) {
		SpringApplication.run(App.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		System.out.println("\nWELCOME TO SPRING DATA\n");
		
		//_auteurDAO.save(new Auteur("riri", "duck", LocalDate.now()));
		
		System.out.println(" -----------Tous les auteurs -----------\n");
		_auteurDAO.findAll().forEach(System.out::println);
		
		System.out.println(" ------------- Les 5 premiers auteurs -----------\n");
		
		Page<Auteur> auteurs =_auteurDAO.findAll(PageRequest.of(0,5));
		
		auteurs.forEach(System.out::println);
		
		System.out.println();
		System.out.println("auteurs.getNumberOfElements() = " + auteurs.getNumberOfElements());
		System.out.println("auteurs.getTotalElements() = " + auteurs.getTotalElements());
		System.out.println("auteurs.getTotalPages() = " + auteurs.getTotalPages());
		System.out.println("auteurs.getContent() = " + auteurs.getContent());
		
		
		System.out.println(" --------------------------------");
		System.out.println("Nombre de livres : " + _livreDAO.count());

		System.out.println(" --------------------------------");
		System.out.println("Livres de 1962 : \n");
		_livreDAO.findByAnneeSortie(1962).forEach(System.out::println);

		System.out.println(" --------------------------------");
		System.out.println("Livres de l'auteur James Ellroy : \n");
		Auteur auteur = _auteurDAO.findByNomAndPrenom("Ellroy","James");
		_livreDAO.findByAuteurs(auteur).forEach(System.out::println);
		
		System.out.println(" --------------------------------");
		System.out.println("Livres de Science Fiction :\n");
		Categorie cat = _categorieDAO.findByNom("Science-fiction");
		_livreDAO.findByCategorie(cat).forEach(System.out::println);

		System.out.println(" --------------------------------");
		System.out.println("Nombre de livres parus entre 1970 et 1980 : \n");
		System.out.println(_livreDAO.countByAnneeSortieBetween(1970, 1980));
		
		System.out.println("Nombre de livres parus entre 1970 et 1980 AVEC QUERY: \n");
		System.out.println(_livreDAO.nomrebreDeLivresParPeriode(1970, 1980));

		System.out.println(" --------------------------------");
		System.out.println("Livres ayant plusieurs auteurs :\n");

		_livreDAO.findByMoreThanOneAuteur().forEach(System.out::println);
		
		System.out.println(" --------------------------------");
		System.out.println("Auteurs encore vivants :\n");
		// _auteurDAO.findAlive().forEach(System.out::println);
		_auteurDAO.findByDecesIsNull().forEach(System.out::println);

		System.out.println(" --------------------------------");
		System.out.println("Auteurs sans livres :\n");
		// _auteurDAO.findByNoBook().forEach(System.out::println);
		_auteurDAO.findByLivresIsEmpty().forEach(System.out::println);
		System.out.println(" --------------------------------");
		System.out.println("Auteurs britaniques :\n");
		Nation n = _nationDAO.findByNom("Grande-Bretagne");

		_auteurDAO.findByNationalite(n).forEach(System.out::println);
		
		System.out.println( " ------------------ CRITERIA : Livre par nom like -------------------------\n");
		
		_livreDAO.chercherLivreParNom("Dahlia").forEach(System.out::println);
	}
}
