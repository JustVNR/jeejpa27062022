package fr.dawan.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.dawan.entities.Nation;
@Repository
public interface NationDAO extends JpaRepository<Nation, Long>{
	
	Nation findByNom(String nom);
}