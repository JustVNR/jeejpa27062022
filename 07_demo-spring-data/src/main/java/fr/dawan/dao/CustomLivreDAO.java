package fr.dawan.dao;

import java.util.List;

import fr.dawan.entities.Livre;

public interface CustomLivreDAO {

	List<Livre> chercherLivreParNom(String titre);
}
