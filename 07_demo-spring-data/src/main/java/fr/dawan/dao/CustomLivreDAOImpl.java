package fr.dawan.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import fr.dawan.entities.Livre;

@Repository
public class CustomLivreDAOImpl implements CustomLivreDAO{

	@Autowired
	private EntityManager em;
	
	@Override
    public List<Livre> chercherLivreParNom(String titre) {
    	
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Livre> cq = cb.createQuery(Livre.class);

        Root<Livre> livre = cq.from(Livre.class);

        Predicate titlePredicate = cb.like(livre.get("titre"), "%" + titre + "%");
        cq.where(titlePredicate);

        TypedQuery<Livre> query = em.createQuery(cq);
        return query.getResultList();
    }
}
