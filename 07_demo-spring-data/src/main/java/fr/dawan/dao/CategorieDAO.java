package fr.dawan.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.dawan.entities.Categorie;
@Repository
public interface CategorieDAO extends JpaRepository<Categorie, Long>{

	Categorie findByNom(String nom);
}
