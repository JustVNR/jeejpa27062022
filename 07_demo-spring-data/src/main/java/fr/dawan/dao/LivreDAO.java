package fr.dawan.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.dawan.entities.Auteur;
import fr.dawan.entities.Categorie;
import fr.dawan.entities.Livre;
@Repository
public interface LivreDAO extends JpaRepository<Livre, Long>, CustomLivreDAO{

	List<Livre> findByAnneeSortie(int annee);
	
	List<Livre> findByAuteurs(Auteur auteur);
	
	List<Livre> findByCategorie(Categorie categorie);
	
	long countByAnneeSortieBetween(int min, int max);
	
	@Query( value = "SELECT COUNT(l) FROM Livre l WHERE l.anneeSortie BETWEEN :min AND :max" )
	long nomrebreDeLivresParPeriode(@Param("min") int min, @Param("max")  int max);
	
	@Query( value = "SELECT l FROM Livre l WHERE SIZE(l.auteurs) > 1 ORDER BY l.anneeSortie" )
	List<Livre> findByMoreThanOneAuteur();
	
}
