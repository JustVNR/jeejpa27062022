package fr.dawan.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.dawan.entities.Auteur;
import fr.dawan.entities.Nation;

@Repository
public interface AuteurDAO extends JpaRepository<Auteur, Long>{

	Auteur findByNomAndPrenom(String nom, String prenom);
	
	List<Auteur> findByDecesIsNull();
	
	List<Auteur> findByLivresIsEmpty();
	
	List<Auteur> findByNationalite(Nation nation);
}
