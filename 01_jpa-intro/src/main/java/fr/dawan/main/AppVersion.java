package fr.dawan.main;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import fr.dawan.entities.Article;

public class AppVersion {

	public static void main(String[] args) {

		EntityManagerFactory emf = null;
		
		EntityManager em = null;
		EntityManager em2 = null;
		EntityTransaction tx = null;
		EntityTransaction tx2 = null;
		
		try {
			
			emf = Persistence.createEntityManagerFactory("jpa-intro-persitence-unit");
			
			em = emf.createEntityManager();
			em2 = emf.createEntityManager();
			
			tx = em.getTransaction();
			tx2 = em2.getTransaction();
			
			System.out.println(" -------------- Insertion d'un nouvel article --------------");
			
			Article article = new Article("MacBook pro", "Apple", 1499);
			
			tx.begin();
			em.persist(article);
			tx.commit(); // Article est cr�� en base
			
			//find By Id
			System.out.println(em.find(Article.class, article.getId()));
			
			Article article2 = em2.find(Article.class, article.getId());
			
			System.out.println("article tx2 : " + article2);
			
			System.out.println(em.find(Article.class, article.getId()));
			
			System.out.println(" -------------- Modification d'un article  dans transaction 1--------------");
			
			tx.begin();
			article.setPrix(1599);
			em.persist(article);
			tx.commit(); // Article modifi� en base
			
			System.out.println(em.find(Article.class, article.getId()));
			
			System.out.println(" -------------- Modification d'un article dans transaction 2 --------------");
			
			tx2.begin();
			article2.setPrix(2599);
			em2.persist(article2);
			tx2.commit(); 
			
			System.out.println(em.find(Article.class, article.getId()));
			System.out.println(em2.find(Article.class, article2.getId()));
			
		}
		catch(Exception e) {
			
			e.printStackTrace();
			
			//Annuler les modification faites en base depuis l'ouverture de la connexion
			tx.rollback(); 
		}
		finally {
			if(em != null) {
				em.close();
			}
			if(emf != null) {
				emf.close();
			}
		}
	}
}
