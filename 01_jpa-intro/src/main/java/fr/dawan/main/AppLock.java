package fr.dawan.main;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.LockModeType;
import javax.persistence.Persistence;

import fr.dawan.entities.Article;

public class AppLock {

	public static void main(String[] args) {

		EntityManagerFactory emf = null;
		
		EntityManager em = null;
		EntityManager em2 = null;
		EntityTransaction tx = null;
		EntityTransaction tx2 = null;
		
		try {
			
			emf = Persistence.createEntityManagerFactory("jpa-intro-persitence-unit");
			
			em = emf.createEntityManager();
			em2 = emf.createEntityManager();
			
			tx = em.getTransaction();
			tx2 = em2.getTransaction();
			
			System.out.println(" -------------- Insertion d'un nouvel article --------------");
			
			Article article = new Article("MacBook pro", "Apple", 1499);
			
			tx.begin();
			em.persist(article);
			tx.commit(); // Article est cr�� en base
			
			System.out.println(em.find(Article.class, article.getId()));
			
			tx2.begin();
			Article article2 = em2.find(Article.class, article.getId(), LockModeType.PESSIMISTIC_READ );
			
			System.out.println("article tx2 : " + article2);
			
			System.out.println(em.find(Article.class, article.getId()));
			
			System.out.println(" -------------- Modification d'un article  dans transaction 1--------------");
			
			tx.begin();
			article.setPrix(1599);
			em.persist(article); // Impossible d'executer cette ligne car article est verouill� par em2
			tx.commit(); 
			
			System.out.println(em.find(Article.class, article.getId()));
			
			tx2.commit();
		}
		finally {
			if(em != null) {
				em.close();
			}
			if(emf != null) {
				emf.close();
			}
		}
	}
}
