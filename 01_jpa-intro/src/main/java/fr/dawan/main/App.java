package fr.dawan.main;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import fr.dawan.entities.Article;
import fr.dawan.enums.Categorie;

public class App {

	public static void main(String[] args) {

		EntityManagerFactory emf = null;
		
		EntityManager em = null;
		EntityTransaction tx = null;
		
		try {
			
			emf = Persistence.createEntityManagerFactory("jpa-intro-persitence-unit");
			
			em = emf.createEntityManager();
			
			tx = em.getTransaction();
			
			tx.begin();
			
			System.out.println(" -------------- Insertion d'un nouvel article --------------");
			
			Article article = new Article("MacBook pro", "Apple", 1499);
			
			article.setCategorie(Categorie.HARDWARE);
			
			em.persist(article);
			
			//find By Id
			System.out.println(em.find(Article.class, article.getId()));
			
			
			System.out.println(" -------------- Modification d'un article --------------");
			
			article.setPrix(1599);
			
			// avec perssit() : ne lance pas forc�ment une requ�te SQL...
			// Attends le tx.commit() pour en faire le moins possible
			//em.persist(article); 
			
			
			// flush : force le commit en BDD sans attendre le commit de la transaxion 
			em.flush();
			article.setPrix(1999);
			System.out.println(em.find(Article.class, article.getId()));
			
			article.setPrix(2500);
			
			//refresh : annule toutes les modifications faites sur l'entit� durant la transaction courante
			// et recharge son �tat � parit de la BDD
			em.refresh(article);
			System.out.println(em.find(Article.class, article.getId()));
			
//			System.out.println(" -------------- Suppression d'un article --------------");
//			
//			em.remove(article);
//			System.out.println(em.find(Article.class, article.getId()));
			
			tx.commit();
			
		}
		catch(Exception e) {
			
			//Annuler les modification faites en base depuis l'ouverture de la connexion
			tx.rollback(); 
		}
		finally {
			if(em != null) {
				em.close();
			}
			if(emf != null) {
				emf.close();
			}
		}
	}
}
