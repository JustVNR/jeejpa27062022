package fr.dawan.listeners;

import java.time.LocalDateTime;

import javax.persistence.PostLoad;
import javax.persistence.PostPersist;
import javax.persistence.PostRemove;
import javax.persistence.PostUpdate;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;

import fr.dawan.entities.Article;

public class ArticleListeners {

	// Listeners

	@PrePersist
	void onPrePersist(Article a) {
		System.out.println(" --- onPrePersist from ArticleListeners -----");
		a.setDateTimeCreation(LocalDateTime.now());
	}

	@PostPersist
	void onPosPersist(Article a) {
		System.out.println(" --- onPosPersist  from ArticleListeners -----");
	}

	@PreUpdate
	void onPreUpdate(Article a) {
		System.out.println(" --- onPreUpdate  from ArticleListeners -----");
	}

	@PostUpdate
	void onPostUpdate(Article a) {
		System.out.println(" --- onPostUpdate  from ArticleListeners -----");
	}

	@PreRemove
	void onPreRemove(Article a) {
		System.out.println(" --- onPreRemove  from ArticleListeners -----");
	}

	@PostRemove
	void onPostRemove(Article a) {
		System.out.println(" --- onPostRemove  from ArticleListeners -----");
	}

	@PostLoad
	void onPostLoad(Article a) {
		System.out.println(" --- onPostLoad  from ArticleListeners -----");
	}
}
