package fr.dawan.entities;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PostLoad;
import javax.persistence.PostPersist;
import javax.persistence.PostRemove;
import javax.persistence.PostUpdate;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Version;

import fr.dawan.enums.Categorie;
import fr.dawan.listeners.ArticleListeners;

@Entity
@Table(name = "T_Articles")
@EntityListeners(ArticleListeners.class)
public class Article {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "articleDesc", length = 50)
	private String description;

	private String marque;

	@Column(name = "PrixUnitaire")
	private double prix;

	@Version
	private int version;

	private LocalDateTime dateTimeCreation;

	private LocalDateTime dateTimeUpdate;

//	@Transient
//	private String nonPesistant;

	private transient String nonPesistant;

	private static String monStatic;

	@Transient
	private final String maConstante = "ma constante non persist�e";

	@Enumerated(EnumType.STRING)
	private Categorie categorie;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getMarque() {
		return marque;
	}

	public void setMarque(String marque) {
		this.marque = marque;
	}

	public double getPrix() {
		return prix;
	}

	public void setPrix(double prix) {
		this.prix = prix;
	}

	public LocalDateTime getDateTimeCreation() {
		return dateTimeCreation;
	}

	public void setDateTimeCreation(LocalDateTime dateTimeCreation) {
		this.dateTimeCreation = dateTimeCreation;
	}

	public LocalDateTime getDateTimeUpdate() {
		return dateTimeUpdate;
	}

	public void setDateTimeUpdate(LocalDateTime dateTimeUpdate) {
		this.dateTimeUpdate = dateTimeUpdate;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public Categorie getCategorie() {
		return categorie;
	}

	public void setCategorie(Categorie categorie) {
		this.categorie = categorie;
	}

	public Article() {
		super();
	}

	public Article(String description, String marque, double prix) {
		super();
		this.description = description;
		this.marque = marque;
		this.prix = prix;
	}

	@Override
	public String toString() {
		return "Article [id=" + id + ", description=" + description + ", marque=" + marque + ", prix=" + prix
				+ ", version=" + version + ", dateTimeCreation=" + dateTimeCreation + ", dateTimeUpdate="
				+ dateTimeUpdate + "]";
	}

	// Listeners
	@PrePersist
	void onPrePersist() {
		System.out.println(" --- onPrePersist -----");
		// this.setDateTimeCreation(LocalDateTime.now());
	}

	@PostPersist
	void onPosPersist() {
		System.out.println(" --- onPosPersist -----");
	}

	@PreUpdate
	void onPreUpdate() {
		System.out.println(" --- onPreUpdate -----");
		this.setDateTimeUpdate(LocalDateTime.now());
	}

	@PostUpdate
	void onPostUpdate() {
		System.out.println(" --- onPostUpdate -----");
	}

	@PreRemove
	void onPreRemove() {
		System.out.println(" --- onPreRemove -----");
	}

	@PostRemove
	void onPostRemove() {
		System.out.println(" --- onPostRemove -----");
	}

	@PostLoad
	void onPostLoad() {
		System.out.println(" --- onPostLoad -----");
	}
}
