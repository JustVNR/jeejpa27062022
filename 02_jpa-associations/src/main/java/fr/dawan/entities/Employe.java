package fr.dawan.entities;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Employe {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	private String nom;

	@Embedded
	private Adresse adresse;

	@Embedded
	@AttributeOverrides({ @AttributeOverride(name = "rue", column = @Column(name = "ruePro")),
			@AttributeOverride(name = "ville", column = @Column(name = "villePro")),

			@AttributeOverride(name = "codePostal", column = @Column(name = "codePostalPro"))

	})
	private Adresse adressePro;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Adresse getAdresse() {
		return adresse;
	}

	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Adresse getAdressePro() {
		return adressePro;
	}

	public void setAdressePro(Adresse adressePro) {
		this.adressePro = adressePro;
	}

	public Employe() {
		super();
	}

	public Employe(String nom, Adresse adresse) {
		super();
		this.nom = nom;
		this.adresse = adresse;
	}

	public Employe(String nom, Adresse adresse, Adresse adressePro) {
		super();
		this.nom = nom;
		this.adresse = adresse;
		this.adressePro = adressePro;
	}

	@Override
	public String toString() {
		return "Employe [id=" + id + ", nom=" + nom + ", adresse=" + adresse + ", adressePro=" + adressePro + "]";
	}
}
