package fr.dawan.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "T_Users")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idUser;

	private String login;

	private String password;

	// @OneToOne
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "idUserInfos", referencedColumnName = "idUserInfos", nullable = false)
	private UserInfos userInfos;

//	@OneToMany(targetEntity = Commande.class, mappedBy = "user")
	@OneToMany
	@JoinTable(name="T_Commandes_User",
	joinColumns = @JoinColumn(name="idUser"),
	inverseJoinColumns = @JoinColumn(name="idCommand"))
	private List<Commande> commandes = new ArrayList<>();
	
	@ManyToMany
	@JoinTable(name="T_Users_Roles",
	joinColumns = @JoinColumn(name="idUser"),
	inverseJoinColumns = @JoinColumn(name="idRole"))
	private List<Role> roles = new ArrayList<>();
	

	public int getIdUser() {
		return idUser;
	}

	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public UserInfos getUserInfos() {
		return userInfos;
	}

	public void setUserInfos(UserInfos userInfos) {
		this.userInfos = userInfos;
	}

	public List<Commande> getCommandes() {
		return commandes;
	}

	public void setCommandes(List<Commande> commandes) {
		this.commandes = commandes;
	}

	
	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	public User() {
		super();
	}

	public User(String login, String password, UserInfos userInfos) {
		super();
		this.login = login;
		this.password = password;
		this.userInfos = userInfos;
	}
	
	public User(String login, String password, UserInfos userInfos, List<Commande> commandes, List<Role> roles) {
		super();
		this.login = login;
		this.password = password;
		this.userInfos = userInfos;
		this.commandes = commandes;
		this.roles = roles;
	}

	@Override
	public String toString() {
		return "User [idUser=" + idUser + ", login=" + login + ", password=" + password + ", userInfos=" + userInfos
				+ "]";
	}
}
