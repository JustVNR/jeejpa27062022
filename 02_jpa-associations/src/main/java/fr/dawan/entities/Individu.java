package fr.dawan.entities;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;

@Entity
public class Individu {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	private String nom;

//	@ManyToOne (cascade = CascadeType.ALL)
//	private Societe societe;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinTable(name = "societe_individu",
	joinColumns = @JoinColumn(name = "individu_id"),
	inverseJoinColumns = @JoinColumn(name = "societe_id"))
	private Societe societe;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Societe getSociete() {
		return societe;
	}

	public void setSociete(Societe societe) {
		this.societe = societe;
	}

	public Individu() {
		super();
	}

	public Individu(String nom, Societe societe) {
		super();
		this.nom = nom;
		this.societe = societe;
	}

	@Override
	public String toString() {
		return "Individu [id=" + id + ", nom=" + nom + ", societe=" + societe + "]";
	}
}
