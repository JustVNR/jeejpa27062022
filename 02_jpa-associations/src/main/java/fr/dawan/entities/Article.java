package fr.dawan.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "T_Articles")
public class Article {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int articleId;

	private double prix;

	@OneToOne
	@JoinTable(name = "T_Articles_Infos_Association",
	joinColumns = @JoinColumn(name = "articleId"),
	inverseJoinColumns = @JoinColumn(name = "articleInfosId"))
	private ArticleInfos articleInfos;

	public int getArticleId() {
		return articleId;
	}

	public void setArticleId(int articleId) {
		this.articleId = articleId;
	}

	public double getPrix() {
		return prix;
	}

	public void setPrix(double prix) {
		this.prix = prix;
	}

	public ArticleInfos getArticleInfos() {
		return articleInfos;
	}

	public void setArticleInfos(ArticleInfos articleInfos) {
		this.articleInfos = articleInfos;
	}

	public Article() {
		super();
	}

	public Article(double prix, ArticleInfos articleInfos) {
		super();
		this.prix = prix;
		this.articleInfos = articleInfos;
	}

	@Override
	public String toString() {
		return "Article [articleId=" + articleId + ", prix=" + prix + ", articleInfos=" + articleInfos + "]";
	}
}
