package fr.dawan.entities;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="T_Civilite")
public class Civilite {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idCivilite;
	
	private String nom;
	
	private String prenom;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="idUserInfos", referencedColumnName = "idUserInfos", nullable = false)
	private UserInfos userInfos;

	public int getIdCivilite() {
		return idCivilite;
	}

	public void setIdCivilite(int idCivilite) {
		this.idCivilite = idCivilite;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public UserInfos getUserInfos() {
		return userInfos;
	}

	public void setUserInfos(UserInfos userInfos) {
		this.userInfos = userInfos;
	}

	public Civilite() {
		super();
	}

	public Civilite(String nom, String prenom) {
		super();
		this.nom = nom;
		this.prenom = prenom;
	}

	@Override
	public String toString() {
		return "Civilite [nom=" + nom + ", prenom=" + prenom + "]";
	}
}
