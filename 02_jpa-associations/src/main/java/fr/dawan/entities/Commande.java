package fr.dawan.entities;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "T_Commands")
public class Commande {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idCommand;

	private LocalDate commandeDate;

	// @ManyToOne
	// @JoinColumn(name = "idUser", referencedColumnName = "idUser", nullable = false)
	
	@ManyToOne
//	@JoinTable(name="T_Commandes_User",
//	joinColumns = @JoinColumn(name="idCommand"),
//	inverseJoinColumns = @JoinColumn(name="idUser"))
	private User user;

	public int getIdCommand() {
		return idCommand;
	}

	public void setIdCommand(int idCommand) {
		this.idCommand = idCommand;
	}

	public LocalDate getCommandeDate() {
		return commandeDate;
	}

	public void setCommandeDate(LocalDate commandeDate) {
		this.commandeDate = commandeDate;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Commande() {
		super();
	}

	public Commande(LocalDate commandeDate, User user) {
		super();
		this.commandeDate = commandeDate;
		this.user = user;
	}

	@Override
	public String toString() {
		return "Commande [idCommand=" + idCommand + ", commandeDate=" + commandeDate + ", user=" + user + "]";
	}
}
