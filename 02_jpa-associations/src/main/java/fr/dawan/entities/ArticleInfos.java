package fr.dawan.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="T_ArticleInfos")
public class ArticleInfos {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int articleInfosId;
	
	private String description;
	
	private String marque;
	
	@OneToOne(mappedBy = "articleInfos")
	private Article article;

	public int getArticleInfosId() {
		return articleInfosId;
	}

	public void setArticleInfosId(int articleInfosId) {
		this.articleInfosId = articleInfosId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getMarque() {
		return marque;
	}

	public void setMarque(String marque) {
		this.marque = marque;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public ArticleInfos() {
		super();
	}

	public ArticleInfos(String description, String marque) {
		super();
		this.description = description;
		this.marque = marque;
	}

	@Override
	public String toString() {
		return "ArticleInfos [articleInfosId=" + articleInfosId + ", description=" + description + ", marque=" + marque
				+ "]";
	}
}
