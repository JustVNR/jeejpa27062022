package fr.dawan.entities;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "T_UserInfos")
public class UserInfos {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idUserInfos;
	
	private String adresse;
	
	private String ville;
	
	private String telephone;
	
	// Relation OneToOne bidirectionnelle avec attribut inverse :
	// - la cl� �trang�re sera dans la table "T_Civilite"
	//- il sera possible d'acc�der � une instance de la classe UserInfos � partir d'une instance de Civilit�
	@OneToOne(mappedBy = "userInfos")
	private Civilite civilite;

	public int getIdUserInfos() {
		return idUserInfos;
	}

	public void setIdUserInfos(int idUserInfos) {
		this.idUserInfos = idUserInfos;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	

	public Civilite getCivilite() {
		return civilite;
	}

	public void setCivilite(Civilite civilite) {
		this.civilite = civilite;
	}

	public UserInfos() {
		super();
	}

	public UserInfos(String adresse, String ville, String telephone) {
		super();
		this.adresse = adresse;
		this.ville = ville;
		this.telephone = telephone;
	}

	public UserInfos(String adresse, String ville, String telephone, Civilite civilite) {
		super();
		this.adresse = adresse;
		this.ville = ville;
		this.telephone = telephone;
		this.civilite = civilite;
	}

	@Override
	public String toString() {
		return "UserInfos [idUserInfos=" + idUserInfos + ", adresse=" + adresse + ", ville=" + ville + ", telephone="
				+ telephone + ", civilite=" + civilite + "]";
	}
}
