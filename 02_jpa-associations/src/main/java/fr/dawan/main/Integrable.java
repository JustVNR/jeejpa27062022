package fr.dawan.main;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import fr.dawan.entities.Adresse;
import fr.dawan.entities.Employe;

public class Integrable {

	public static void main(String[] args) {

		EntityManagerFactory emf = null;

		EntityManager em = null;
		EntityTransaction tx = null;

		try {

			emf = Persistence.createEntityManagerFactory("jpa-asso");

			em = emf.createEntityManager();

			tx = em.getTransaction();

			tx.begin();

			Employe employe = new Employe("riri", new Adresse("rue du riri", "Amiens", 80000));
			
			employe.setAdressePro(new Adresse("rue du boulo", "Amiens", 80000));
			em.persist(employe);
			
			tx.commit();
			
			System.out.println(em.find(Employe.class, employe.getId()));

		} catch (Exception e) {

			e.printStackTrace();
			e.getMessage();
			tx.rollback();
		} finally {
			if (em != null) {
				em.close();
			}
			if (emf != null) {
				emf.close();
			}
		}
	}
}
