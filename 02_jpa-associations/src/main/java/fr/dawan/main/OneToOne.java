package fr.dawan.main;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import fr.dawan.entities.Article;
import fr.dawan.entities.ArticleInfos;
import fr.dawan.entities.Civilite;
import fr.dawan.entities.User;
import fr.dawan.entities.UserInfos;

public class OneToOne {

	public static void main(String[] args) {

		EntityManagerFactory emf = null;

		EntityManager em = null;
		EntityTransaction tx = null;

		try {

			emf = Persistence.createEntityManagerFactory("jpa-asso");

			em = emf.createEntityManager();

			tx = em.getTransaction();

			tx.begin();

			UserInfos ui = new UserInfos("14 rue du riri", "Amiens", "0745854123");

			// em.persist(ui); // Inutile si CascadeType.ALL ou CascadeType.PERSIST

			User user = new User("riri@gmail.com", "pwd", ui);

			em.persist(user);

			System.out.println(em.find(User.class, user.getIdUser()));

			System.out.println(" ------------------ BIDIRECTIONNELLE ----------------------");

			// Relation OneToOne bidirectionnelle entre userInfos et Civilite

			UserInfos ui2 = new UserInfos("14 rue du loulou", "Toulouse", "0745654223");

			Civilite civi = new Civilite("Duck", "Loulou");

			civi.setUserInfos(ui2);

			em.persist(civi);

			User user2 = new User("loulou@gmail.com", "pwd", ui2);

			em.persist(user2);

			ui2.setCivilite(civi);

			// UserInfos correspondant � la civilite d'Id 1

			System.out.println(em.find(Civilite.class, 1).getUserInfos());

			System.out.println(" ------------------ OneToOne avec table de jointure ----------------------");

			ArticleInfos ai = new ArticleInfos("Macbook air", "Apple");
			em.persist(ai);
			
			Article article = new Article(1499, ai);
			
			em.persist(article);
			
			tx.commit();

		} catch (Exception e) {

			// Annuler les modification faites en base depuis l'ouverture de la connexion
			tx.rollback();
		} finally {
			if (em != null) {
				em.close();
			}
			if (emf != null) {
				emf.close();
			}
		}
	}
}
