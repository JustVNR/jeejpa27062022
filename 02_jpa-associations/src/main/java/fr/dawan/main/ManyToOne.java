package fr.dawan.main;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import fr.dawan.entities.Individu;
import fr.dawan.entities.Societe;

public class ManyToOne {

	public static void main(String[] args) {

		EntityManagerFactory emf = null;

		EntityManager em = null;
		EntityTransaction tx = null;

		try {

			emf = Persistence.createEntityManagerFactory("jpa-asso");

			em = emf.createEntityManager();

			tx = em.getTransaction();

			tx.begin();

			Societe amazon = new Societe("Amazon");

			Individu individu = new Individu("riri", amazon);
			Individu individu2 = new Individu("fifi", amazon);
			Individu individu3 = new Individu("loulou", new Societe("Google"));

			em.persist(individu);
			em.persist(individu2);
			em.persist(individu3);

			tx.commit();

			System.out.println(" --------------- Liste des soci�t�s --------------------");

			em.createQuery("SELECT c FROM Societe c", Societe.class).getResultList().forEach(System.out::println);

		} catch (Exception e) {

			tx.rollback();
		} finally {
			if (em != null) {
				em.close();
			}
			if (emf != null) {
				emf.close();
			}
		}
	}
}
