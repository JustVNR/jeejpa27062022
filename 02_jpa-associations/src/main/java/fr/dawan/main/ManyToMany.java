package fr.dawan.main;

import java.util.ArrayList;
import java.util.Arrays;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import fr.dawan.entities.Role;
import fr.dawan.entities.User;
import fr.dawan.entities.UserInfos;

public class ManyToMany {

	public static void main(String[] args) {

		EntityManagerFactory emf = null;

		EntityManager em = null;
		EntityTransaction tx = null;

		try {

			emf = Persistence.createEntityManagerFactory("jpa-asso");

			em = emf.createEntityManager();

			tx = em.getTransaction();

			tx.begin();

			User user = new User("riri@gmail.com", "mdp", new UserInfos("rue du riri", "Nantes", "0645785429"));
			User user2 = new User("fifi@gmail.com", "mdp", new UserInfos("rue du fifi", "Paris", "0644535429"));
			em.persist(user);
			em.persist(user2);
			
			Role role1 = new Role("admin");
			Role role2 = new Role("user");
			
			em.persist(role1);
			em.persist(role2);
			
			user.setRoles(new ArrayList<Role>(Arrays.asList(role1)));
			user2.setRoles(new ArrayList<Role>(Arrays.asList(role1, role2)));
			
			tx.commit();
			
			em.find(User.class, user2.getIdUser()).getRoles().forEach(System.out::println);
			
		} catch (Exception e) {

			tx.rollback();
		} finally {
			if (em != null) {
				em.close();
			}
			if (emf != null) {
				emf.close();
			}
		}
	}
}
