package fr.dawan.main;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import fr.dawan.entities.Commande;
import fr.dawan.entities.User;
import fr.dawan.entities.UserInfos;

public class OneToMany {

	public static void main(String[] args) {

		EntityManagerFactory emf = null;

		EntityManager em = null;
		EntityTransaction tx = null;

		try {

			emf = Persistence.createEntityManagerFactory("jpa-asso");

			em = emf.createEntityManager();

			tx = em.getTransaction();

			tx.begin();

			User user = new User("riri@gmail.com", "mpd", new UserInfos("rue du riri", "Nantes", "0612365874"));

			em.persist(user);

			Commande command1 = new Commande(LocalDate.now(), user);

			em.persist(command1);

			Commande command2 = new Commande(LocalDate.now(), user);

			em.persist(command2);
			
			List<Commande> commandes = new ArrayList<Commande>();
			
			commandes.add(command1);
			commandes.add(command2);
			
			user.setCommandes(commandes);
			
			em.persist(user);
			
			tx.commit();
			
			System.out.println(" ---------------- Commandes du user --------------------- ");
			
			user.getCommandes().forEach(System.out::println);
			
		} catch (Exception e) {

			tx.rollback();
		} finally {
			if (em != null) {
				em.close();
			}
			if (emf != null) {
				emf.close();
			}
		}
	}
}
