package fr.dawan.entities;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "compte_discriminator", discriminatorType = DiscriminatorType.STRING, length = 15)
@DiscriminatorValue("CB")
public class CompteBancaire extends AbstractEntity {

	private String numero;

	private double solde;

	private LocalDate date;

	@ManyToOne
	Client client;

	@OneToMany(mappedBy = "cb")
	private List<Operation> operations;

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public double getSolde() {
		return solde;
	}

	public void setSolde(double solde) {
		this.solde = solde;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public List<Operation> getOperations() {
		return operations;
	}

	public void setOperations(List<Operation> operations) {
		this.operations = operations;
	}

	public CompteBancaire() {
		super();

	}

	public CompteBancaire(String numero, double solde, LocalDate date) {
		this();
		this.numero = numero;
		this.solde = solde;
		this.date = date;
	}

	public CompteBancaire(String numero, double solde, LocalDate date, Client client) {
		this();
		this.numero = numero;
		this.solde = solde;
		this.date = date;
		this.client = client;
	}

	public void retrait(double montant) throws Exception {
		if (montant <= 0 || montant > solde) {
			throw new Exception();
		}

		solde -= montant;
	}

	public void depot(double montant) throws Exception {
		if (montant <= 0) {
			throw new Exception();
		}

		solde += montant;
	}

	@Override
	public String toString() {

		return this.getClass().getAnnotation(DiscriminatorValue.class).value() + " [numero=" + numero + ", solde="
				+ solde + ", date=" + date + ", client=" + client.getEmail() + "]";
	}
}
