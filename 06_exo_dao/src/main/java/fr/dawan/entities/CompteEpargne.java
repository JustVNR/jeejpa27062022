package fr.dawan.entities;

import java.time.LocalDate;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("CE")
public class CompteEpargne extends CompteBancaire {

	private double rate;

	public double getRate() {
		return rate;
	}

	public void setRate(double rate) {
		this.rate = rate;
	}

	public CompteEpargne() {
		super();
	}

	public CompteEpargne(String numero, double solde, LocalDate date, double rate) {
		super(numero, solde, date);
		this.rate = rate;
	}

	public CompteEpargne(String numero, double solde, LocalDate date, double rate, Client client) {
		super(numero, solde, date, client);
		this.rate = rate;
	}
}
