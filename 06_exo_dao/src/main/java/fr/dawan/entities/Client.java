package fr.dawan.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

@Entity
public class Client extends AbstractEntity {

	private String nom;

	@Column(unique = true)
	private String email;

	@OneToMany(mappedBy = "client")
	List<CompteBancaire> comptes = new ArrayList<CompteBancaire>();

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<CompteBancaire> getComptes() {
		return comptes;
	}

	public void setComptes(List<CompteBancaire> comptes) {
		this.comptes = comptes;
	}

	public Client() {
		super();
	}

	public Client(String nom, String email) {
		this();
		this.nom = nom;
		this.email = email;
	}

	@Override
	public String toString() {

		String strComptes = comptes.size() > 0 ? ", comptes=\n" : "";

		for (CompteBancaire compte : comptes) {
			strComptes += "\t- " + compte.getClass().getAnnotation(DiscriminatorValue.class).value() + " N� "
					+ compte.getNumero() + "\n";
		}
		return "Client [id = " + getId()  + ", nom=" + nom + ", email=" + email + strComptes + "]";
	}
}
