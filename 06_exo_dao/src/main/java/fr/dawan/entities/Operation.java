package fr.dawan.entities;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;

import fr.dawan.enums.OperationType;

@Entity
public class Operation extends AbstractEntity {
	
	@ManyToOne
	private CompteBancaire cb;

	private LocalDate date;

	private double montant;

	@Enumerated(EnumType.STRING)
	private OperationType type;

	public CompteBancaire getCb() {
		return cb;
	}

	public void setCb(CompteBancaire cb) {
		this.cb = cb;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public double getMontant() {
		return montant;
	}

	public void setMontant(double montant) {
		this.montant = montant;
	}

	public OperationType getType() {
		return type;
	}

	public void setType(OperationType type) {
		this.type = type;
	}

	public Operation() {
		super();
		this.date = LocalDate.now();
	}

	public Operation(CompteBancaire cb, double montant, OperationType type) {
		this();
		this.cb = cb;
		this.montant = montant;
		this.type = type;
	}

	@Override

	public String toString() {
		return "Operation [cb=" + cb.getNumero() + ", date=" + date + ", montant=" + montant + ", type=" + type + "]";
	}
}
