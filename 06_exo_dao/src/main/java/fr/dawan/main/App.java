package fr.dawan.main;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import fr.dawan.dao.ClientDAO;
import fr.dawan.dao.CompteBancaireDAO;
import fr.dawan.dao.OperationDAO;
import fr.dawan.entities.Client;
import fr.dawan.entities.CompteBancaire;
import fr.dawan.entities.CompteEpargne;

public class App {

	public static void main(String[] args) {

		EntityManagerFactory emf = null;

		EntityManager em = null;

		try {

			emf = Persistence.createEntityManagerFactory("jpa-dao");

			em = emf.createEntityManager();

			/* Cr�er une application autorisant des clients � effectuer des d�pots,
			 *  des retraits sur l'un de leurs comptes bancaires courant ou compte �pargne.
			 *  
			 * Il devra �galement �tre possible d'effectuer des virements entre 2 comptes diff�rents. 
			 *  
			 * Un Client est d�fini par son nom, son email et une liste de comptes bancaires.
			 * Un compte bancaire est d�fini par :
			 * - un numero de compte
			 * - un solde
			 * - une date de cr�ation
			 * - un client
			 * - une liste d'op�rations effectu�es (depot, retrait)
			 * 
			 * Un compte �pargne est un compte bancaire se caract�risant par son taux de rendement
			 * 
			 * Un compte bancaire permet d'effectuer des d�p�ts et des retraits
			 * 
			 * Une op�ration est d�finie par :
			 * 
			 * - un compte bancaire
			 * - une date
			 * - un montant
			 * - un type (DEPOT ou RETRAIT)
			 * 
			 * Cr�er une couche d'acc�s au donn�es comprenant  :
			 * 
			 * - un GenericDAO devant permettre de faire un C.R.U.D sur une entit� g�n�rique
			 * - un ClientDAO devant permettre de cr�er, modifier, supprimer et lister des
			 * clients
			 * - un CompteBancaireDAO devant permettre de cr�er, modifier, supprimer
			 * et lister des comptes bancaires
			 * - un OperationDAO devant permettre
			 * 		- de persister une Op�ration
			 * 		- d'effectuer des retraits et d�pots sur un compte
			 * 		- ainsi que des virements entre 2 comptes bancaires diff�rents
			 */
			
			ClientDAO _clientDAO = new ClientDAO(em);
			CompteBancaireDAO _compteBancaireDAO = new CompteBancaireDAO(em);
			OperationDAO _operationDAO = new OperationDAO(em);
			
			Client cl1 = new Client("riri", "riri@gmail.com");
			
			_clientDAO.saveOrUpdate(cl1);
			
			cl1.setNom("fifi");
			_clientDAO.saveOrUpdate(cl1);
			
			
			Client cl2 = new Client("loulou", "loulou@gmail.com");
			
			_clientDAO.saveOrUpdate(cl2);
			
			_clientDAO.remove(cl1);
			
			CompteBancaire cb1 = new CompteBancaire("num_cb1", 1000.0, LocalDate.now(), cl2);
			CompteBancaire cb2 = new CompteEpargne("num_cb2", 25000.0, LocalDate.now(), 0.2, cl2);
			
			
			List<CompteBancaire> comptes = new ArrayList<CompteBancaire>();
			
			comptes.add(cb2);
			comptes.add(cb1);
			
			cl2.setComptes(comptes);
			
			_clientDAO.saveOrUpdate(cl2);
			
			_compteBancaireDAO.saveOrUpdate(cb1);
			_compteBancaireDAO.saveOrUpdate(cb2);
			
			System.out.println(_compteBancaireDAO.findById(cb2.getId()));
			System.out.println(_compteBancaireDAO.findById(cb1.getId()));
			_operationDAO.depot(cb2, 1000.0);
			_operationDAO.retrait(cb1, 200.0);
			
			System.out.println(_compteBancaireDAO.findById(cb2.getId()));
			System.out.println(_compteBancaireDAO.findById(cb1.getId()));
			System.out.println(_clientDAO.findById(cl2.getId()));
			
			
			_operationDAO.virement(cb1, cb2, 300.0);
			
			System.out.println(" ------------------- Apr�s virement  --------------------- ");
			System.out.println(_compteBancaireDAO.findById(cb2.getId()));
			System.out.println(_compteBancaireDAO.findById(cb1.getId()));
			
			

		} catch (Exception e) {

			e.printStackTrace();
			e.getMessage();

		} finally {
			if (em != null) {
				em.close();
			}
			if (emf != null) {
				emf.close();
			}
		}
	}
}
