package fr.dawan.dao;

import java.util.List;

import javax.persistence.EntityManager;

import fr.dawan.entities.Client;

public class ClientDAO extends GenericDAO<Client> {

	private EntityManager em;

	public EntityManager getEm() {
		return em;
	}

	public void setEm(EntityManager em) {
		this.em = em;
	}

	public ClientDAO(EntityManager em) {
		super(em, Client.class);
		this.em = em;
	}

	public List<Client> findByName(String name) {

		return em.createQuery("SELECT c FROM Client c WHERE c.nom LIKE :name", Client.class)
				.setParameter("name", "%" + name + "%").getResultList();
	}

}
