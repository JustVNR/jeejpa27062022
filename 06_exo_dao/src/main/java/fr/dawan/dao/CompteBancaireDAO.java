package fr.dawan.dao;

import javax.persistence.EntityManager;

import fr.dawan.entities.CompteBancaire;

public class CompteBancaireDAO extends GenericDAO<CompteBancaire>{
	
	private EntityManager em;

	public EntityManager getEm() {
		return em;
	}

	public void setEm(EntityManager em) {
		this.em = em;
	}

	public CompteBancaireDAO(EntityManager em) {
		super(em, CompteBancaire.class);
		this.em = em;
	}
}
