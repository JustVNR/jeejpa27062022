package fr.dawan.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import fr.dawan.entities.AbstractEntity;

public class GenericDAO<T extends AbstractEntity> {

	protected EntityManager em;

	private Class<T> classEntity;

	public EntityManager getEm() {
		return em;
	}

	public void setEm(EntityManager em) {
		this.em = em;
	}

	public GenericDAO(EntityManager em, Class<T> classEntity) {
		super();
		this.em = em;
		this.classEntity = classEntity;
	}

	public void saveOrUpdate(T elm) throws Exception {

		EntityTransaction tx = em.getTransaction();

		try {
			tx.begin();

			em.persist(elm);

			tx.commit();
		} catch (Exception e) {

			tx.rollback();

			throw e;
		}
	}

	public void remove(long id) {

		EntityTransaction tx = em.getTransaction();

		try {

			T elm = em.find(classEntity, id);

			if (elm == null)
				return;

			tx.begin();

			em.remove(elm);

			tx.commit();
		} catch (Exception e) {

			tx.rollback();

			throw e;
		}
	}

	public void remove(T elm) {

		EntityTransaction tx = em.getTransaction();

		try {

			tx.begin();

			em.remove(elm);

			tx.commit();
		} catch (Exception e) {

			tx.rollback();

			throw e;
		}
	}

	public T findById(long id) {
		return em.find(classEntity, id);
	}

	public List<T> findAll() {
		return em.createQuery("SELECT e FROM " + classEntity.getName() + " e", classEntity).getResultList();
	}

	public List<T> findAll(int start, int end) {
		return em.createQuery("SELECT e FROM " + classEntity.getName() + " e", classEntity).setFirstResult(start)
				.setMaxResults(end).getResultList();
	}
	
	public Long count() {
		
		return em.createQuery("SELECT COUNT(e) FROM " + classEntity.getName() + " e", Long.class).getSingleResult();
	}
}
