package fr.dawan.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import fr.dawan.entities.CompteBancaire;
import fr.dawan.entities.Operation;
import fr.dawan.enums.OperationType;

public class OperationDAO extends GenericDAO<Operation> {

	private EntityManager em;

	public EntityManager getEm() {
		return em;
	}

	public void setEm(EntityManager em) {
		this.em = em;
	}

	public OperationDAO(EntityManager em) {
		super(em, Operation.class);
		this.em = em;
	}

	public void depot(CompteBancaire c, double montant) throws Exception {

		EntityTransaction tx = em.getTransaction();

		try {
			tx.begin();

			c.depot(montant);

			em.persist(c);

			em.persist(new Operation(c, montant, OperationType.DEPOT));

			tx.commit();

		} catch (Exception e) {
			
			tx.rollback();
			
			throw e;
		}
	}

	public void retrait(CompteBancaire c, double montant) throws Exception {

		EntityTransaction tx = em.getTransaction();

		try {
			tx.begin();

			c.retrait(montant);

			em.persist(c);

			em.persist(new Operation(c, montant, OperationType.RETRAIT));

			tx.commit();

		} catch (Exception e) {
			tx.rollback();
			throw e;
		}
	}
	
	public void virement(CompteBancaire c1, CompteBancaire c2, double montant) throws Exception {
		
		EntityTransaction tx = em.getTransaction();

		try {
			tx.begin();
			
			c1.retrait(montant);
			c2.depot(montant);
			
			em.persist(c1);
			em.persist(new Operation(c1, montant, OperationType.RETRAIT));
			
			em.persist(c2);
			em.persist(new Operation(c2, montant, OperationType.DEPOT));
			
			tx.commit();

		} catch (Exception e) {
			tx.rollback();
			throw e;
		}
	}
}
