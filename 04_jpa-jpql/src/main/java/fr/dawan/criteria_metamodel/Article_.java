package fr.dawan.criteria_metamodel;

import javax.persistence.metamodel.StaticMetamodel;

import fr.dawan.entities.Article;

@StaticMetamodel(Article.class)
public class Article_ {
	
	public static final String PRIX = "prix";
	
}
