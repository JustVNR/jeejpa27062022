package fr.dawan.main;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import fr.dawan.entities.Article;

public class _02_EagerVsLazy {

	public static void main(String[] args) {

		EntityManagerFactory emf = null;

		EntityManager em = null;
	
		try {

			emf = Persistence.createEntityManagerFactory("jpa-jpql");

			em = emf.createEntityManager();

			Article article = em.find(Article.class, 1L);
			
			System.out.println(" -------------- LAZY VS EAGER ---------------------");
			
			System.out.println(article.getMarque());

		} catch (Exception e) {

			e.printStackTrace();
			e.getMessage();
			
		} finally {
			if (em != null) {
				em.close();
			}
			if (emf != null) {
				emf.close();
			}
		}
	}
}
