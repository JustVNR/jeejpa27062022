package fr.dawan.main;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import fr.dawan.entities.Article;
import fr.dawan.entities.Marque;

public class _05_Nommees {

	@SuppressWarnings("unchecked")
	public static void main(String[] args) {

		EntityManagerFactory emf = null;

		EntityManager em = null;

		try {

			emf = Persistence.createEntityManagerFactory("jpa-jpql");

			em = emf.createEntityManager();

			System.out.println(" --------------- REQUETES NOMMEES---------------------- ");

			// Articles dont le prix est inf�rieur � celui pass� en param�tre
			em.createNamedQuery("Article.prixMax", Article.class).setParameter("prix", 100.0).getResultList()
					.forEach(System.out::println);

			// Articles dont le prix est compris dans une fourchette pass�e en param�tres
			em.createNamedQuery("Article.prixFourchette", Article.class).setParameter("prixMin", 100.0)
					.setParameter("prixMax", 1400.0).getResultList().forEach(System.out::println);
			
			// Toutes les marques
			em.createNamedQuery("Marque.getAll", Marque.class).getResultList().forEach(System.out::println);

		} catch (Exception e) {

			e.printStackTrace();
			e.getMessage();
		} finally {
			if (em != null) {
				em.close();
			}
			if (emf != null) {
				emf.close();
			}
		}
	}
}
