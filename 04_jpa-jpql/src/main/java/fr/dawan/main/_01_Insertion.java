package fr.dawan.main;

import java.time.LocalDate;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import fr.dawan.entities.Article;
import fr.dawan.entities.Fournisseur;
import fr.dawan.entities.Marque;

public class _01_Insertion {

	public static void main(String[] args) {

		EntityManagerFactory emf = null;

		EntityManager em = null;
		EntityTransaction tx = null;

		try {

			emf = Persistence.createEntityManagerFactory("jpa-jpql");

			em = emf.createEntityManager();

			tx = em.getTransaction();

			tx.begin();

			Article article = new Article("MacBook Air", 1399.0, LocalDate.now());
			Article article2 = new Article("Souris", 80.0, LocalDate.of(2020, 5, 25));
			Article article3 = new Article("Tv 4k", 1500.0, LocalDate.of(2021, 11, 12));
			Article article4 = new Article("Cable USB_%2", 20.0, LocalDate.of(2021, 3, 12));
			Article article5 = new Article("PC_HP", 999.0, LocalDate.of(2019, 05, 12));
			Article article6 = new Article("DDR4", 80.0, LocalDate.of(2021, 11, 12));

			Marque apple = new Marque("Apple");
			Marque hp = new Marque("HP");
			Marque microsoft = new Marque("Microsoft");
			Marque samsung = new Marque("Samsung");
			
			em.persist(apple);
			article.setMarque(apple);
			em.persist(article);
			
			em.persist(microsoft);
			article2.setMarque(microsoft);
			em.persist(article2);
			
			em.persist(samsung);
			article3.setMarque(samsung);
			em.persist(article3);
			
			em.persist(hp);
			
			em.persist(article4);
			em.persist(article5);
			em.persist(article6);
			
			Fournisseur fournisseur = new Fournisseur();
			fournisseur.setNom("Cdiscount");
			fournisseur.getArticles().add(article6);
			fournisseur.getArticles().add(article);
			em.persist(fournisseur);
			
			Fournisseur fournisseur2 = new Fournisseur();
			fournisseur2.setNom("Amazon");
			fournisseur2.getArticles().add(article);
			fournisseur2.getArticles().add(article2);
			em.persist(fournisseur2);
			
			tx.commit();

		} catch (Exception e) {

			e.printStackTrace();
			e.getMessage();
			tx.rollback();
		} finally {
			if (em != null) {
				em.close();
			}
			if (emf != null) {
				emf.close();
			}
		}
	}
}
