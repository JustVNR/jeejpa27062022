package fr.dawan.main;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class _04_SQL {

	@SuppressWarnings("unchecked")
	public static void main(String[] args) {

		EntityManagerFactory emf = null;

		EntityManager em = null;

		try {

			emf = Persistence.createEntityManagerFactory("jpa-jpql");

			em = emf.createEntityManager();

			System.out.println(" --------------- SQL NATIF ------------------");
			
			// Description et prix des articles dont le prix est inf�rieur � celui pass� en
			// param�tre
			List<Object[]> lst = em.createNativeQuery("SELECT a.prix, a.description FROM articles a WHERE a.prix < :prix")
					.setParameter("prix", 100.0).getResultList();

			for (Object[] objects : lst) {
				for (Object result : objects) {
					System.out.print(result + " ");
				}
				System.out.println();
			}

		} catch (Exception e) {

			e.printStackTrace();
			e.getMessage();
		} finally {
			if (em != null) {
				em.close();
			}
			if (emf != null) {
				emf.close();
			}
		}
	}
}
