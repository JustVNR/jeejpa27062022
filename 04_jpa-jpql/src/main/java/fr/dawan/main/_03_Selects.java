package fr.dawan.main;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import fr.dawan.entities.Article;
import fr.dawan.entities.Fournisseur;
import fr.dawan.entities.Marque;

public class _03_Selects {

	public static void main(String[] args) {

		EntityManagerFactory emf = null;

		EntityManager em = null;
		
		try {

			emf = Persistence.createEntityManagerFactory("jpa-jpql");

			em = emf.createEntityManager();

			// Liste des articles
			// em.createQuery("SELECT a FROM Article a",
			// Article.class).getResultList().forEach(System.out::println);
			em.createQuery("FROM Article", Article.class).getResultList().forEach(System.out::println);

			System.out.println(" ------------ PAGINATION----------------------");
			// Liste des articles � partir du 3i�me et maximum 2 r�sultats
			em.createQuery("FROM Article", Article.class).setFirstResult(2).setMaxResults(2).getResultList()
					.forEach(System.out::println);
			System.out.println(" ------------ FIN PAGINATION----------------------");

			// Description et prix de chaque article
			List<Object[]> lstObj = em.createQuery("SELECT a.description, a.prix FROM Article a").getResultList();

			for (Object[] obj : lstObj) {
				System.out.println("Description : " + obj[0] + " , Prix : " + obj[1]);
			}

			// Articles dont le prix est sup�rieur � 80
			em.createQuery("SELECT a FROM Article a WHERE a.prix > 80.0", Article.class).getResultList()
					.forEach(System.out::println);

			// Articles dont le prix n'est pas inf�rieur � 80
			em.createQuery("SELECT a FROM Article a WHERE NOT a.prix < 80.0", Article.class).getResultList()
					.forEach(System.out::println);

			// Articles dont le prix est inf�rieur � 100 ou sup�rieur � 1400
			em.createQuery("SELECT a FROM Article a WHERE a.prix < 100.0 OR a.prix > 1400.0", Article.class)
					.getResultList().forEach(System.out::println);

			// Articles dont la description est "Tv 4k"
			em.createQuery("SELECT a FROM Article a WHERE a.description = 'Tv 4k'", Article.class).getResultList()
					.forEach(System.out::println);

			// Articles ajout�s avant le premier novembre 2021
			em.createQuery("SELECT a FROM Article a WHERE a.dateAjout < '2021-11-01'", Article.class).getResultList()
					.forEach(System.out::println);

			// Quart du prix des Articles ajout�s avant le premier novembre 2021
			em.createQuery("SELECT a.prix*0.5 FROM Article a WHERE a.dateAjout < '2021-11-01'", Double.class)
					.getResultList().forEach(System.out::println);

			// Marques sans articles
			em.createQuery("SELECT m FROM Marque m WHERE m.articles IS EMPTY", Marque.class).getResultList()
					.forEach(System.out::println);

			// Article sans marques
			em.createQuery("SELECT a FROM Article a WHERE a.marque IS NULL", Article.class).getResultList()
					.forEach(System.out::println);

			// Article ajout�s entre le 01/10/2021 et le 01/01/2022
			em.createQuery("SELECT a FROM Article a WHERE a.dateAjout BETWEEN '2021-10-01' AND '2022-01-01'",
					Article.class).getResultList().forEach(System.out::println);

			// Articles dont le prix vaut 20, 40, ou 80
			em.createQuery("FROM Article a WHERE a.prix IN (20.0,40.0,80.0)", Article.class).getResultList()
					.forEach(System.out::println);

			// Articles dont la description commence par "T"
			em.createQuery("FROM Article a WHERE a.description LIKE 'T%'", Article.class).getResultList()
					.forEach(System.out::println);

			// Articles dont la description commence par "Cable USB_%"
			em.createQuery("FROM Article a WHERE a.description LIKE 'Cable USB@_@%%' ESCAPE '@'", Article.class)
					.getResultList().forEach(System.out::println);

			// Articles dont le prix est inf�rieur � 1500 tri�s du plus au moins cher par
			// ordre alphab�tique de desciption
			em.createQuery("FROM Article a WHERE a.prix < 1500 ORDER BY a.prix DESC, a.description", Article.class)
					.getResultList().forEach(System.out::println);

			System.out.println(" ----------------------- Param�tres ------------------------");

			// Articles dont le prix est inf�rieur � celui pass� en param�tre
			em.createQuery("FROM Article a WHERE a.prix < :prix ", Article.class).setParameter("prix", 1500.0)
					.getResultList().forEach(System.out::println);

			em.createQuery("FROM Article a WHERE a.prix < ?1 ", Article.class).setParameter(1, 1500.0).getResultList()
					.forEach(System.out::println);

			// Articles dont la description est comme celle pass�e en param�tre
			em.createQuery("FROM Article a WHERE a.description LIKE :nom", Article.class).setParameter("nom", "Tv%")
					.getResultList().forEach(System.out::println);

			System.out.println(" ----------------------- Fonctions ------------------------");

			// Nombres d'articles
			System.out.println(em.createQuery("SELECT COUNT(a) FROM Article a", Long.class).getSingleResult());

			// Nombres d'articles d'articles dont le prix est inf�rieur � celui pass� en
			// param�tre
			System.out.println(em.createQuery("SELECT COUNT(a) FROM Article a WHERE a.prix < :prix", Long.class)
					.setParameter("prix", 40.0).getSingleResult());

			System.out.println(" ----------------------- Fonctions chaines de caract�res ------------------------");

			// Description et prix de chaque article sous fomre d'une chaine de caract�res

			em.createQuery("SELECT CONCAT('Description :', a.description, ' , Prix :', a.prix) FROM Article a",
					String.class).getResultList().forEach(System.out::println);

			// 3 premi�res lettres en majuscule de la description de chaque article
			em.createQuery("SELECT SUBSTRING(UPPER(a.description), 1, 3) FROM Article a", String.class).getResultList()
					.forEach(System.out::println);

			// Longeur de la description de cahque Article
			em.createQuery("SELECT LENGTH(a.description) FROM Article a", Integer.class).getResultList()
					.forEach(System.out::println);

			// LOCATE : chercher une sous chaine dans une chaine de caract�res et retourne
			// sa position

			// Position e la chaine "our" dans la description de chaque article
//			em.createQuery("SELECT LOCATE('our', a.description) FROM Article a",
//					Integer.class).getResultList().forEach(System.out::println);

			em.createQuery("SELECT CONCAT( a.description, ' : ', LOCATE('our', a.description)) FROM Article a",
					String.class).getResultList().forEach(System.out::println);

			// Nombre d'articles dont la marque est renseign�e
			em.createQuery("SELECT SIZE(m.articles) FROM Marque m WHERE m.articles IS NOT EMPTY", Integer.class)
					.getResultList().forEach(System.out::println);

			// prix et nombre d'articles group�s par prix comptant au moins 2 articles
			em.createQuery(
					"SELECT CONCAT('Prix : ', a.prix, ' : ', COUNT(a)) FROM Article a GROUP BY a.prix HAVING Count(a) > 1",
					String.class).getResultList().forEach(System.out::println);

			System.out.println(" ------------- JOIN --------------");

			// Article dont la marque est "Apple"
			em.createQuery("SELECT a FROM Article a WHERE a.marque.nom = 'Apple'", Article.class).getResultList()
					.forEach(System.out::println);

			em.createQuery("SELECT a FROM Article a JOIN a.marque m WHERE m.nom = 'Apple'", Article.class)
					.getResultList().forEach(System.out::println);

			System.out.println(" ------------- SOUS REQUETE --------------");

			// Fournisseurs vendant des articles coutant 1399.0
			em.createQuery(
					"SELECT f FROM Fournisseur f WHERE (SELECT a FROM Article a WHERE a.prix = 1399.0) MEMBER OF f.articles",
					Fournisseur.class).getResultList().forEach(System.out::println);

			System.out.println(" ------------- JOIN FETCH --------------");

			// Marques ayant au moins un article

//			System.out.println(" ------ AVEC : FORCE LE MODE EAGER");
//
//			em.createQuery("SELECT DISTINCT m FROM Marque m JOIN FETCH m.articles a", Marque.class).getResultList()
//					.forEach((m) -> {
//
//						List<Article> articles = m.getArticles();
//
//						for (Article article : articles) {
//							System.out.println(m.getNom() + " : " + article.getDescription());
//						}
//					});

			System.out.println(" ------ SANS FETCH : MODE LAZY");

			em.createQuery("SELECT DISTINCT m FROM Marque m JOIN m.articles a", Marque.class).getResultList().forEach((m) -> {

				List<Article> articles = m.getArticles();

				for (Article article : articles) {
					System.out.println(m.getNom() + " : " + article.getDescription());
				}
			});
			
			

		} catch (Exception e) {

			e.printStackTrace();
			e.getMessage();
		} finally {
			if (em != null) {
				em.close();
			}
			if (emf != null) {
				emf.close();
			}
		}
	}
}
