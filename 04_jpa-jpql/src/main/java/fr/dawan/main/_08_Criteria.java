package fr.dawan.main;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import fr.dawan.criteria_metamodel.Article_;
import fr.dawan.entities.Article;

public class _08_Criteria {

	@SuppressWarnings("unchecked")
	public static void main(String[] args) {

		EntityManagerFactory emf = null;

		EntityManager em = null;

		try {

			emf = Persistence.createEntityManagerFactory("jpa-jpql");

			em = emf.createEntityManager();

			System.out.println(" --------------- CRITERIA QUERY---------------------- ");
			
			// Articles dont le prix est inf�rieur � 100 par ordre de prix;
			
			CriteriaBuilder cb = em.getCriteriaBuilder();
			
			CriteriaQuery<Article> cq = cb.createQuery(Article.class);
			
			Root<Article> root = cq.from(Article.class);
			
			// cq.select(root).where(cb.lessThan(root.get("prix"), 100.0)).orderBy(cb.asc(root.get("prix")));
			
			cq.select(root).where(cb.lessThan(root.get(Article_.PRIX), 100.0)).orderBy(cb.asc(root.get(Article_.PRIX)));
			
			em.createQuery(cq).getResultList().forEach(System.out::println);
			
		} catch (Exception e) {

			e.printStackTrace();
			e.getMessage();
		} finally {
			if (em != null) {
				em.close();
			}
			if (emf != null) {
				emf.close();
			}
		}
	}
}
