package fr.dawan.main;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import fr.dawan.entities.Article;

public class _07_UpdateAndRemove {

	@SuppressWarnings("unchecked")
	public static void main(String[] args) {

		EntityManagerFactory emf = null;

		EntityManager em = null;

		try {

			emf = Persistence.createEntityManagerFactory("jpa-jpql");

			em = emf.createEntityManager();

			System.out.println(" --------------- UPDATE AND REMOVE---------------------- ");

			em.getTransaction().begin();

			// UPDATE

			// Baisser de 1 le prix des articles dont le prix est inf�rieur � celui pass� en
			// param�tre
			em.createQuery("FROM Article a WHERE a.prix < :prix", Article.class).setParameter("prix", 100.0)
					.getResultList().forEach((a) -> {
						System.out.println(a);
						a.setPrix(a.getPrix() - 1);
						System.out.println(a);
					});

//			em.createQuery("UPDATE Article a SET a.prix = a.prix - 1 WHERE a.prix < :prix").setParameter("prix", 100.0)
//					.executeUpdate();

			// em.clear();
			
			em.getTransaction().commit();
//			System.out.println(" ------------------------------ ");
//			articles.forEach(System.out::println);

			// DELETE

			// Supprimer les articles dont le prix vaut celui pass� en param�tre

//			em.getTransaction().begin();
//
//			em.createQuery("DELETE FROM Article a WHERE a.prix = :prix").setParameter("prix", 15.0).executeUpdate();
//
//			em.getTransaction().commit();
//
//			em.clear();
//
//			em.createQuery("FROM Article", Article.class).getResultList().forEach(System.out::println);

		} catch (Exception e) {

			em.getTransaction().rollback();
			e.printStackTrace();
			e.getMessage();
		} finally {
			if (em != null) {
				em.close();
			}
			if (emf != null) {
				emf.close();
			}
		}
	}
}
