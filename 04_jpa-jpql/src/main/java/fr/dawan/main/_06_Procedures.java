package fr.dawan.main;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.ParameterMode;
import javax.persistence.Persistence;
import javax.persistence.StoredProcedureQuery;

public class _06_Procedures {

	@SuppressWarnings("unchecked")
	public static void main(String[] args) {

		EntityManagerFactory emf = null;

		EntityManager em = null;

		try {

			emf = Persistence.createEntityManagerFactory("jpa-jpql");

			em = emf.createEntityManager();

			System.out.println(" --------------- PROCEDURES---------------------- ");

			StoredProcedureQuery spq = em.createStoredProcedureQuery("GET_COUNT_BY_PRIX");
			
			spq.registerStoredProcedureParameter("montant", Double.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter("nbArticles", Integer.class, ParameterMode.OUT);
			
			spq.setParameter("montant", 80.0);
			
			spq.execute();
			
			int nbArticles = (Integer)spq.getOutputParameterValue("nbArticles");
			
			System.out.println(nbArticles);

		} catch (Exception e) {

			e.printStackTrace();
			e.getMessage();
		} finally {
			if (em != null) {
				em.close();
			}
			if (emf != null) {
				emf.close();
			}
		}
	}
}
