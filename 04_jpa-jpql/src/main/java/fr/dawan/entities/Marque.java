package fr.dawan.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedNativeQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "marques")
@NamedNativeQuery(name = "Marque.getAll", query = "SELECT m.id, m.nom FROM marques m", resultClass = Marque.class)
public class Marque {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(length = 50, nullable = false)
	private String nom;

	@OneToMany(mappedBy = "marque", cascade = CascadeType.ALL)
	private List<Article> articles = new ArrayList<>();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public List<Article> getArticles() {
		return articles;
	}

	public void setArticles(List<Article> articles) {
		this.articles = articles;
	}

	public Marque() {
		super();
	}

	public Marque(String nom) {
		super();
		this.nom = nom;
	}

	@Override
	public String toString() {
		return "Marque : " + nom;
	}
}
