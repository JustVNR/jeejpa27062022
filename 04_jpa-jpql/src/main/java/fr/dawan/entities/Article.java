package fr.dawan.entities;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "articles")

@NamedQueries({ @NamedQuery(name = "Article.prixMax", query = "SELECT a FROM Article a WHERE a.prix < :prix"),
		@NamedQuery(name = "Article.prixFourchette", query = "SELECT a FROM Article a WHERE a.prix BETWEEN :prixMin AND :prixMax") })
public class Article {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(nullable = false)
	private String description;

	@Column(nullable = false)
	private double prix;

	@Column(name = "date_ajout", nullable = false)
	private LocalDate dateAjout;

	@ManyToOne(fetch = FetchType.LAZY) // Par d�faut fetch EAGER pour un ManyToOne
	private Marque marque;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getPrix() {
		return prix;
	}

	public void setPrix(double prix) {
		this.prix = prix;
	}

	public LocalDate getDateAjout() {
		return dateAjout;
	}

	public void setDateAjout(LocalDate dateAjout) {
		this.dateAjout = dateAjout;
	}

	public Marque getMarque() {
		return marque;
	}

	public void setMarque(Marque marque) {
		this.marque = marque;
	}

	public Article() {
		super();
	}

	public Article(String description, double prix, LocalDate dateAjout) {
		super();
		this.description = description;
		this.prix = prix;
		this.dateAjout = dateAjout;
	}

	public Article(String description, double prix, LocalDate dateAjout, Marque marque) {
		super();
		this.description = description;
		this.prix = prix;
		this.dateAjout = dateAjout;
		this.marque = marque;
	}

	@Override
	public String toString() {
		return "Article [description=" + description + ", prix=" + prix + ", dateAjout=" + dateAjout + ", marque="
				+ marque + "]";
	}
}
